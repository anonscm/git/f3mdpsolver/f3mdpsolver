% example script for gradient descent

% the output of this script for myf3mdp is given in file
% test_gradient_descent_output

clc
clear all;
close all;

add=1; % additive rewards

% h_f=1 means that the horizon is infinite and approximated by the finite
% horizon it_max
% h_f=0 allows to compute the stopping time based on a criterion about the
% absolute error
h_f=1; % finite horizon 
it_max=20; % horizon
gamma=0.9; % discount

% unuseful parameters when h_f=1
r_max=0;
eps=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHOOSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate a random F^3MDP
% n_max_s=3; % maximum number of possible values for state variables
% n_max_a=3; % maximum number of possible values for action variables
% n_states=3; % number of state variables
% n_actions=3; % number of action variables
% n_rewards=3; % number of rewards functions
% n_max_pa=9; % maximum number of parents for factors in the factor graph
% arc_synch=1; % synchronous arcs allowed
% [f3mdp]=rand_f3mdp(n_states,n_actions,n_rewards,n_max_s,n_max_a,n_max_pa,arc_synch);

% or use myf3mdp
[f3mdp,~]=myf3mdp();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% parameters for libDAI
INPUT.meth='BP';
INPUT.param='[updates=SEQFIX,tol=1e-1,maxiter=1000,logdomain=1,inference=SUMPROD,damping=0,verbose=0]';

% gmdp=0 means the F^3MDP is not a GMDP (in the case of a GMDP, if GDMP=1,
% the evaluation code will be faster because there is no need of artificial
% factors in the factor graph)
gmdp=0;

% inputs for gradient descent
INPUT.gamma=gamma;
INPUT.h_f=h_f;
INPUT.eps=eps;
INPUT.it_max=it_max;
INPUT.add=add;
INPUT.it_max_gd=1000;
INPUT.eps_gd_V=10^(-2);
INPUT.eps_gd_theta=10^(-2);
INPUT.eps_gd_grad=10^(-2);
INPUT.mydisp=0; % display
INPUT.gmdp=gmdp;
INPUT.eps_df=10^(-2);
INPUT.eps1=10^(-4);
INPUT.eps2=0.99;
INPUT.it_max_rl=10;
INPUT.type_step='wolfe';
INPUT.step=10;
INPUT.borne_max_pas_opt=1000;
INPUT.save=0;
INPUT.nom_fich='';

% options for fminbnd (used to optimize the step in gradient descent)
INPUT.options=optimset('TolX',10^(-2),'MaxIter',1000,'Display','iter');

% initialisation with the uniform policy
Dloc0=init_policy(f3mdp);
[theta0]=DlocToTheta(Dloc0,0);

% GRADIENT DESCENT
% for the parallelized version add _par at the end of the name of the
% function
tic
[Dloc,OUTPUT]=gradient_descent_reparam_par(f3mdp,theta0,INPUT);
time_gd=toc
OUTPUT

% the problem is small and can be solved exactly with the MDP toolbox
tic
[P,R,Dloc_glob,P_0]=f3mdp_globalize_par(f3mdp,Dloc,add);
[Vex, policy, iter, cpu_time] = mdp_policy_iteration(P, R, gamma);
time_exact=toc
 
% value of the exact policy for P0
Vex_P0=sum(P_0.*Vex)
 
% exact evaluation of the policy from gradient descent
[Vf_cor]=eval_sto_mdp2(P_0,P,R,Dloc_glob,gamma); 

% relative error
RE=abs(Vf_cor-Vex_P0)/Vex_P0

% determinization of the policy from gradient descent
[Y,I]=max(Dloc_glob);
I

% evaluation of the determinized policy
V_det = mdp_eval_policy_matrix(P, R, gamma, I');
V_det_P0=sum(P_0.*V_det)

% comparison with the exacte policy
policy' % compare with I
RE_det=abs(V_det_P0-Vex_P0)/Vex_P0

% note that gradient descent compute a policy with a given structure (here
% random) and not a global policy as the exact resolution



function[f3mdp,Dloc]=myf3mdp()

% generates an example of F^3MDP for non regression tests
% see myf3mdp_example.pdf in the doc folder
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OUTPUT
%%  f3mdp : the F^3MDP
%%  Dloc (optional output) : factored stochastic policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f3mdp.n_states=3;
f3mdp.n_actions=2;
f3mdp.n_rewards=2;

f3mdp.S=[2,2,3];
f3mdp.A=[2,2];

f3mdp.Ps=cell(1,f3mdp.n_states);
f3mdp.Ps{1}=1;
f3mdp.Ps{2}=[1,2];
f3mdp.Ps{3}=[2 3];
f3mdp.Ps2=cell(1,f3mdp.n_states);
f3mdp.Ps2{1}=2;
f3mdp.Ps2{2}=[];
f3mdp.Ps2{3}=[];
f3mdp.Pa=cell(1,f3mdp.n_states);
f3mdp.Pa{1}=1;
f3mdp.Pa{2}=2;
f3mdp.Pa{3}=2;

f3mdp.Ds=cell(1,f3mdp.n_actions);
f3mdp.Ds{1}=[1,2];
f3mdp.Ds{2}=[2,3];
f3mdp.Da=cell(1,f3mdp.n_actions);
f3mdp.Da{1}=2;
f3mdp.Da{2}=[];

f3mdp.Rs=cell(1,f3mdp.n_rewards);
f3mdp.Rs{1}=1;
f3mdp.Rs{2}=[2,3];
f3mdp.Ra=cell(1,f3mdp.n_rewards);
f3mdp.Ra{2}=2;
f3mdp.Ra2=cell(1,f3mdp.n_rewards);


% Construction of P_0 
f3mdp.P_0=cell(1,f3mdp.n_states);
f3mdp.P_0{1}=[0.2;0.8];
f3mdp.P_0{2}=[0.3;0.7];
f3mdp.P_0{3}=[0.5;0.4;0.1];

% Construction of P_loc
f3mdp.Ploc=cell(1,f3mdp.n_states);

f3mdp.Ploc{1}=zeros(2,2,2,2);
f3mdp.Ploc{1}(:,:,1,1)=[0.9,0.2;0.1,0.8];
f3mdp.Ploc{1}(:,:,2,1)=[0.1,0.5;0.9,0.5];
f3mdp.Ploc{1}(:,:,1,2)=[0.3,0.5;0.7,0.5];
f3mdp.Ploc{1}(:,:,2,2)=[0.1,0.4;0.9,0.6];

f3mdp.Ploc{2}=zeros(2,2,2,2);
f3mdp.Ploc{2}(:,:,1,1)=[0.9,0.2;0.1,0.8];
f3mdp.Ploc{2}(:,:,2,1)=[0.1,0.3;0.9,0.7];
f3mdp.Ploc{2}(:,:,1,2)=[0.4,0.3;0.6,0.7];
f3mdp.Ploc{2}(:,:,2,2)=[0.1,0.5;0.9,0.5];

f3mdp.Ploc{3}=zeros(3,2,3,2);
f3mdp.Ploc{3}(:,:,1,1)=[0.4,0.2;0.4,0.7;0.2,0.1];
f3mdp.Ploc{3}(:,:,2,1)=[0.3,0.2;0.4,0.7;0.3,0.1];
f3mdp.Ploc{3}(:,:,3,1)=[0.3,0.5;0.4,0.1;0.3,0.4];
f3mdp.Ploc{3}(:,:,1,2)=[0.3,0.2;0.4,0.7;0.3,0.1];
f3mdp.Ploc{3}(:,:,2,2)=[0.4,0.4;0.4,0.5;0.2,0.1];
f3mdp.Ploc{3}(:,:,3,2)=[0.3,0.2;0.4,0.7;0.3,0.1];


% Construction of R_loc
f3mdp.Rloc=cell(1,f3mdp.n_rewards);
f3mdp.Rloc{1}=[0.7;0.1];
f3mdp.Rloc{2}=zeros(2,3,2);
f3mdp.Rloc{2}(:,:,1)=[0.6,0.4,0.2;0.1,0.3,0.5];
f3mdp.Rloc{2}(:,:,2)=[0.1,0.3,0.5;0.2,0.5,0.5];

% Construction of D_loc
Dloc=cell(1,f3mdp.n_actions);
Dloc{1}=zeros(2,2,2,2);
Dloc{1}(:,:,1,1)=[0.1,0.6;0.9,0.4];
Dloc{1}(:,:,2,1)=[0.2,0.6;0.8,0.4];
Dloc{1}(:,:,1,2)=[0.1,0.3;0.9,0.7];
Dloc{1}(:,:,2,2)=[0.5,0.3;0.5,0.7];

Dloc{2}=zeros(2,2,3);
Dloc{2}(:,:,1)=[0.1,0.2;0.9,0.8];
Dloc{2}(:,:,2)=[0.5,0.2;0.5,0.8];
Dloc{2}(:,:,3)=[0.1,0.3;0.9,0.7];



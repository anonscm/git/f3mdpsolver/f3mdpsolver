% example script for evaluation

% the output of this script for myf3mdp is given in file
% test_eval_output

clc
clear all;
close all;

add=1; % additive rewards
mydisp=0; % no display

% h_f=1 means that the horizon is infinite and approximated by the finite
% horizon it_max
% h_f=0 allows to compute the stopping time based on a criterion about the
% absolute error
h_f=1; % finite horizon 
it_max=20; % horizon
gamma=0.9; % discount

% unuseful parameters when h_f=1
r_max=0;
eps=0;
er_abs=0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CHOOSE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate a random F^3MDP, and a stationary policy Dloc to evaluate
%n_max_s=2; % maximum number of possible values for state variables
%n_max_a=2; % maximum number of possible values for action variables
%n_states=3; % number of state variables
%n_actions=3; % number of action variables
%n_rewards=3; % number of rewards functions
%n_max_pa=9; % maximum number of parents for factors in the factor graph
%arc_synch=0; % no synchronous arcs
%[f3mdp,Dloc]=rand_f3mdp(n_states,n_actions,n_rewards,n_max_s,n_max_a,n_max_pa,arc_synch);

% or generate myf3mdp (example) and an associated policy 
% see myf3mdp_example.pdf in the doc folder
[f3mdp,Dloc]=myf3mdp();
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% parameters for libDAI
meth='BP';
param='[updates=SEQFIX,tol=1e-1,maxiter=1000,logdomain=1,inference=SUMPROD,damping=0,verbose=0]';

% gmdp=0 means the F3MDP is not a GMDP (in the case of a GMDP, if GDMP=1,
% the evaluation code will be faster because there is no need of artificial
% factors in the factor graph)
gmdp=0;

% creation of the factor graph associated with the F^3MDP
fg=create_factor_graph(f3mdp,Dloc,gmdp,it_max);

% EVALUATION with libDAI
tic
[V2,T]=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp); 
toc
V2

% EVALUATION with Monte-Carlo
n_simu=4000;
% must precise orders and ordera because there are synchronous arcs
orders=[2,1,3];
ordera=[2,1];
tic
[Vmc,~,~]=eval_mc_par(f3mdp,Dloc,n_simu,gamma,h_f,eps,it_max,r_max,er_abs,add,orders,ordera);
toc
Vmc

% the problem is small so the policy can be evaluated exactly with the MDP toolbox
% (infinite horizon)
[P,R,policy,P_0]=f3mdp_globalize_par(f3mdp,Dloc,add);
V=eval_sto_mdp2(P_0,P,R,policy,gamma);
V

% relative error
RE=abs(V-V2)/V 
% the relative error decreases when it_max increases

% relative error (Monte-Carlo)
RE_mc=abs(V-Vmc)/V 

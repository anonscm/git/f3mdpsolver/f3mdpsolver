% counterexample showing that the best factored policy may not be deterministic

clc
clear all;

n_states=2;
n_actions=1;
n_rewards=1;
n_s=2;
n_a=2;
n_max_pa=4;
h_f=1;

add=1;
gmdp=0;

gamma=0.9;
it_max=40;

f3mdp.S=2*ones(1,n_states);
f3mdp.A=2*ones(1,n_actions);
f3mdp.n_states=n_states;
f3mdp.n_actions=n_actions;
f3mdp.n_rewards=n_rewards;

f3mdp.P_0=cell(1,n_states);
f3mdp.P_0{1}=[0.5;0.5];
f3mdp.P_0{2}=[0.5;0.5];

f3mdp.Ps{1}=2;
f3mdp.Ps{2}=1;
f3mdp.Pa{1}=1;
f3mdp.Pa{2}=1;
f3mdp.Ps2=cell(1,n_states);
f3mdp.Ploc{1}=zeros(2,2,2);
f3mdp.Ploc{1}(:,:,1)=[0.7,0.6;0.3,0.4];
f3mdp.Ploc{1}(:,:,2)=[0.5,0.2;0.5,0.8];
f3mdp.Ploc{2}=zeros(2,2,2);
f3mdp.Ploc{2}(:,:,1)=[0.6,0.6;0.4,0.4];
f3mdp.Ploc{2}(:,:,2)=[0.5,0.3;0.5,0.7];

f3mdp.Rs{1}=1;
f3mdp.Ra{1}=1;
f3mdp.Ra2=cell(1,f3mdp.n_rewards);
f3mdp.Rloc{1}=[0.45,0.80;1,0.7];

f3mdp.Ds{1}=2;
f3mdp.Da=cell(1,n_actions);

f3mdp.ordres=[];
f3mdp.ordrea=[];


% compute P and R
[P,R,P_0]=f3mdp_globalize_PR(f3mdp,add);

%%%%%%%%%%%%%%%%%%%% evaluation (infinite horizon)
% exact evaluation of the factored deterministic policies with this structure
D1=cell(1,n_actions);
D1{1}=[1,1;0,0];
D2=cell(1,n_actions);
D2{1}=[1,0;0,1];
D3=cell(1,n_actions);
D3{1}=[0,1;1,0];
D4=cell(1,n_actions);
D4{1}=[0,0;1,1];

[pi]=policy_globalize(f3mdp,D1);
[V1]=eval_sto_mdp2(P_0,P,R,pi,gamma)

[pi]=policy_globalize(f3mdp,D2);
[V2]=eval_sto_mdp2(P_0,P,R,pi,gamma)

[pi]=policy_globalize(f3mdp,D3);
[V3]=eval_sto_mdp2(P_0,P,R,pi,gamma)

[pi]=policy_globalize(f3mdp,D4);
[V4]=eval_sto_mdp2(P_0,P,R,pi,gamma)


% example of factored stochastic policy with this structure which is better
D5{1}=[0,0.5;1,0.5];

[pi]=policy_globalize(f3mdp,D5);
V5=eval_sto_mdp2(P_0,P,R,pi,gamma)


% global optimal policy
[Vex, policy_opt, iter, cpu_time] = mdp_policy_iteration(P, R, gamma);
policy_opt
Vex_P0=sum(Vex.*P_0)

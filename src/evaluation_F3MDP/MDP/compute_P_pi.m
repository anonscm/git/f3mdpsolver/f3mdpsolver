function[P_pi,R_pi]=compute_P_pi(P,R,pi)

% function that computes the transition matrix P_pi and the reward matrix
% R_pi of an MDP for policy pi (useful to evaluate a stochastic policy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% P : P(s'|s,a) order of the dimensions : S^{t} then S^{t+1} then A^t
%% R : R(s,a) order of the dimensions : S^t then A^t
%% pi : pi(s,a), order of the dimensions : A^t then S^t
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% P_pi : P_pi(s'|s), order of the dimensions : S^{t} then S^{t+1}
%% R_pi : column vector corresponding to R_pi(s)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_s=size(P,1);
n_a=size(pi,1);
P_pi=sum(bsxfun(@times,P,reshape(pi',[n_s,1,n_a])),3);
R_pi=sum(R.*(pi'),2);
function[V]=eval_sto_mdp2(P_0,P,R,pi,gamma)

% function that evaluates exactly a stochastic policy for an MDP
% uses Bellman equation
% considers an initial distribution on states (for a fixed initial state,
% use a deterministic initial distribution P0)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% P_0 : column vector corresponding to the initial distribution on states
%% P : P(s'|s,a) order of the dimensions : S^{t} then S^{t+1} then A^t
%% R : R(s,a) order of the dimensions : S^t then A^t
%% pi : pi(s,a), order of the dimensions : A^t then S^t
%% gamma : discount factor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% V : value of pi for the initial distribution P_0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[P_pi,R_pi]=compute_P_pi(P,R,pi);

%V=P_0'*inv(eye(size(P_pi))-gamma*P_pi)*R_pi;

% faster and more precise?
V=P_0'*((eye(size(P_pi))-gamma*P_pi)\R_pi);

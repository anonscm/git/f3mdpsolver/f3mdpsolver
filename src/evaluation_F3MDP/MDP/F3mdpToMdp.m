function[P,R]=F3mdpToMdp(f3mdp)

% function that transforms a F^3MDP with one state variable, one action
% variable and one reward function into an MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT : MDP
%% P : transition matrix
%% R : reward matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (f3mdp.n_states==1 && f3mdp.n_actions==1 && f3mdp.n_rewards==1)
    % in P we must have S^t then S^{t+1} then A^t
    P=permute(f3mdp.Ploc{1},[2,1,3]);
    % in R we must have S^t then A^t
    R=f3mdp.Rloc{1};
    % verifications
    mdp_check(P , R)
else
    error('An F^3MDP cannot be transformed into an MDP if it has more than one state variable, action variable, or reward function');
end
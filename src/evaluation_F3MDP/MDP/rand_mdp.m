function[f3mdp]=rand_mdp(n_max_s,n_max_a)

% generates a random MDP in form of an F^3MDP
% only tables are random
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% n_max_s : maximum number of possible states
%% n_max_a : maximum number of possible actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%%  f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('../../utils');

f3mdp.n_states=1;
f3mdp.n_actions=1;
f3mdp.n_rewards=1;

% randi : with replacement
% randperm : without replacement

f3mdp.S=n_max_s;
f3mdp.A=n_max_a;

f3mdp.Ps=cell(1,f3mdp.n_states);
f3mdp.Ps{1}=1;

f3mdp.Ps2=cell(1,f3mdp.n_states);

f3mdp.Pa=cell(1,f3mdp.n_states);
f3mdp.Pa{1}=1;

f3mdp.Ds=cell(1,f3mdp.n_actions);
f3mdp.Ds{1}=1;

f3mdp.Da=cell(1,f3mdp.n_actions);

f3mdp.Rs=cell(1,f3mdp.n_rewards);
f3mdp.Rs{1}=1;

f3mdp.Ra=cell(1,f3mdp.n_rewards);
f3mdp.Ra{1}=1;

f3mdp.Ra2=cell(1,f3mdp.n_rewards);

% Construction of P_0
f3mdp.P_0=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    f3mdp.P_0{i}=rand(f3mdp.S(i),1);
    f3mdp.P_0{i}=normalize_mat(f3mdp.P_0{i},1);
end

% Construction of Ploc
f3mdp.Ploc=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    n_v=length(f3mdp.Ps{i})+length(f3mdp.Ps2{i})+length(f3mdp.Pa{i});
    dim=zeros(1,n_v);
    dim(1)=f3mdp.S(i);
    cpt=2;
    for k=1:length(f3mdp.Ps{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ps2{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps2{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Pa{i})
        dim(cpt)=f3mdp.A(f3mdp.Pa{i}(k));
        cpt=cpt+1;
    end
    f3mdp.Ploc{i}=rand(dim);
    f3mdp.Ploc{i}=normalize_mat(f3mdp.Ploc{i},1);
end

% Construction of Dloc
for i=1:f3mdp.n_actions
        n_v=length(f3mdp.Ds{i})+length(f3mdp.Da{i});
        dim=zeros(1,n_v);
        dim(1)=f3mdp.A(i);
        cpt=2;
        for k=1:length(f3mdp.Ds{i})
            dim(cpt)=f3mdp.S(f3mdp.Ds{i}(k));
            cpt=cpt+1;
        end
        for k=1:length(f3mdp.Da{i})
            dim(cpt)=f3mdp.A(f3mdp.Da{i}(k));
            cpt=cpt+1;
        end 
        f3mdp.Dloc{i}=rand(dim);
        f3mdp.Dloc{i}=normalize_mat(f3mdp.Dloc{i},1);
end

% Construction of Rloc
f3mdp.Rloc=cell(1,f3mdp.n_rewards);
for i=1:f3mdp.n_rewards
    n_v=length(f3mdp.Rs{i})+length(f3mdp.Ra{i});
    dim=zeros(1,n_v);
    cpt=1;
    for k=1:length(f3mdp.Rs{i})
        dim(cpt)=f3mdp.S(f3mdp.Rs{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ra{i})
        dim(cpt)=f3mdp.A(f3mdp.Ra{i}(k));
        cpt=cpt+1;
    end
    f3mdp.Rloc{i}=squeeze(rand([dim,1])); 
end


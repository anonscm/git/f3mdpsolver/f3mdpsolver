function[policy_GMDP]=PolicyGmdp(Dloc_det,f3mdp)

% transformation of a deterministic policy of a F^3MDP which is a GMDP into
% a policy in the format of the GMDP toolbox
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% Dloc_det : the deterministic policy
%% f3mdp : the F^3MDP which is a GMDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% policy_GDMP : the policy in GMDP format
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% precaution
Dloc_det=determinisation(f3mdp,Dloc_det);

n=length(f3mdp.S);

% neighbourhood
N=f3mdp.Ds; 

policy_GMDP=cell(1,n);
for i=1:n
    taille_etats=f3mdp.S(N{i});
    nb_glob=prod(taille_etats);
    policy_GMDP{i}=zeros(1,nb_glob);
    for j=1:nb_glob
        etat_pa=n_vec(j,taille_etats);
        ind=num2cell(etat_pa);
        policy_GMDP{i}(j)=find(Dloc_det{i}(:,ind{:}));                
    end
end



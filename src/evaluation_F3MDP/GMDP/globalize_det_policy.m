function[policy]=globalize_det_policy(det_policy,f3mdp)

% function that globalizes a deterministic factored policy (obtained as
% output of the MF-API code)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% det_policy : list of length the number of action variables
% det_policy{i} : vector representing the action to choose for each
% combination of local states
%% f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% policy : vector of length the number of possible global states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_glob_states=prod(f3mdp.S);
policy=zeros(1,n_glob_states);

for i=1:n_glob_states
   vec=n_vec(i,f3mdp.S); 
   % vector of the local actions
   actions=zeros(1,f3mdp.n_actions);
   for a=1:f3mdp.n_actions
       vec_a=vec(f3mdp.Ds{a});
       state=vec_n(vec_a,f3mdp.S(f3mdp.Ds{a}));
       actions(a)=det_policy{a}(state);
   end
   % transforming into a global action
   policy(i)=vec_n(actions,f3mdp.A);
end
function [Dloc,Ds,Da] = greedy_policy_GMDP(f3mdp,eps)

% function that gives a greedy policy for a GMDP (the reward at node i
% depends only on the action of node i)
% a greedy policy is a deterministic factored policy that maximizes the
% immediate reward 
% we then have Ds=Rs, and Da=[]
% if parameter eps is given, this policy is transformed into a stochastic
% policy with eps instead of 0
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% in the case of the epidemiologic problem, the greedy policy consists in
% never treating
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% eps : facultative parameter for transforming into stochastic policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% Dloc : greedy policy
%% Ds, Da : structure of the greedy policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n = f3mdp.n_states;

Dloc = cell(1,n);
Ds=cell(1,n);
Da=cell(1,n);

if (nargin>1) % if stochastic policy with eps
    for i = 1:n
      Ds{i}=f3mdp.Rs{i};  
      dim=f3mdp.S(f3mdp.Rs{i});
      dim2=num2cell(dim);
      Dloc{i} = eps*ones(2,dim2{:}); % we put epsilon everywhere
      for j = 1:prod(dim)
        vect=n_vec(j,dim);
        vect2=num2cell(vect);
        [~,k] = max(f3mdp.Rloc{i}(vect2{:},:));
        Dloc{i}(k,vect2{:}) = 1-eps;
      end
    end

else % if pure deterministic policy
    for i = 1:n
      Ds{i}=f3mdp.Rs{i};  
      dim=f3mdp.S(f3mdp.Rs{i});
      dim2=num2cell(dim);
      Dloc{i} = zeros(2,dim2{:}); % we put 0 everywhere
      for j = 1:prod(dim)
        vect=n_vec(j,dim);
        vect2=num2cell(vect);
        [~,k] = max(f3mdp.Rloc{i}(vect2{:},:));
        Dloc{i}(k,vect2{:}) = 1;
      end
    end
end

function[Ploc,Rloc,S,A,Adj]=F3mdpToGmdp(f3mdp)

% transformation of an F^3MDP representing a GMDP into a GMDP in the format
% of the GMDP toolbox
% (then I only need to transform the adjacence matrix with mat_2_graph in
% Scilab)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT : GMDP
%% Ploc : transition matrix
%% Rloc : reward matrix
%% S : vector of the sizes of the state spaces
%% A : vector of the sizes of the action spaces
%% Adj : adjacence matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

S=f3mdp.S;
A=f3mdp.A;

n=length(f3mdp.S);

% neighbourhood
N=f3mdp.Ps; % same as Rs normally

% creation of the adjacence matrix G
Adj=zeros(n,n);
for i=1:n
    Adj(N{i},i)=ones(length(N{i}),1);
end

% creation of Ploc and Rloc
% Ploc(i) = hypermat([|A_i| |S_i| |S_N(i)|]);
% f3mdp.Ploc(i) =  |S(i)|x prod_{j dans Ps(i)} |S(j)|x |A(i)|

% creation of Rloc
% Rloc(i) =  mat([|A_i| |S_N(i)|])
% f3mdp.Rloc(i) =  prod_{i dans Rs(k)} |S(i)|x |A(i)|

Ploc=cell(1,n);
Rloc=cell(1,n);
for i=1:n
    taille_etats=f3mdp.S(N{i});
    nb_glob=prod(taille_etats);
    for j=1:nb_glob
        etat_pa=n_vec(j,taille_etats);
        ind=num2cell(etat_pa);
        
        for a=1:f3mdp.A(i)
            Ploc{i}(a,:,j)=f3mdp.Ploc{i}(:,ind{:},a);
            Rloc{i}(a,j)=f3mdp.Rloc{i}(ind{:},a);
        end
    end
end




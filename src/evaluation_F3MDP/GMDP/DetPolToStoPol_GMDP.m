function[sto_pol]=DetPolToStoPol_GMDP(f3mdp,det_pol)

% function that transform a deterministic policy, like the ones returned by
% MF-API, into a 'stochastic' policy (with only 0 and 1)
% for GMDPs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% det_pol : list of length n_actions
% det_pol(i) : vector of action numbers
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUPUT
%% sto_pol : list of length n_actions
% sto_pol(i) : matrix of size |A(j)|x prod_{i dans Ds(j)}
%                  |S(i)|x prod_{j2 dans Da(j)} |A(j2)|
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(f3mdp.n_actions~=length(det_pol))
    disp('DetPolToStoPol_PDMG : problem of length.');
end

sto_pol=cell(1,f3mdp.n_actions);

for i=1:f3mdp.n_actions
   n_v=length(det_pol{i});
   size_v=f3mdp.S(f3mdp.Ds{i});
   size_v_cell=num2cell(size_v);
   sto_pol{i}=zeros(f3mdp.A(i),size_v_cell{:});
   for j=1:n_v
       etat_pa=n_vec(j,size_v);
       etat_pa_cell=num2cell(etat_pa);
       action=det_pol{i}(j);
       sto_pol{i}(action,etat_pa_cell{:})=1;
   end
end
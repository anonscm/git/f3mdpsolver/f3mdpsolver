function[f3mdp]=rand_gmdp(n,n_max_s,n_max_a,v)

% generates a random GMDP in form of an F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% n : number of state variables, action variables, and reward functions
%% n_max_s : maximum number of possible states
%% n_max_a : maximum number of possible actions
%% v : number of neighbours for the factors in the factor graph
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%%  f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


f3mdp.n_states=n;
f3mdp.n_actions=n;
f3mdp.n_rewards=n;

% randi : with replacement
% randperm : without replacement

if (n==1) % case of an MDP
    f3mdp.S=n_max_s;
    f3mdp.A=n_max_a;
else
    f3mdp.S=randi([2,n_max_s],[1,n]);
    f3mdp.A=randi([2,n_max_a],[1,n]); 
end

N=cell(1,f3mdp.n_states);
nb_par=randi([1,min(n,v-3)],[1,n]);
for i=1:n
    N{i}=randperm(n,nb_par(i));
    % need of i in order to use the GMDP toolbox
    % i add it if it is not 
    if(~myismember(i,N{i}))
        N{i}=[N{i},i];
    end
    N{i}=sort(N{i});
end

f3mdp.Ps=N;
f3mdp.Ds=N;
f3mdp.Rs=N;

f3mdp.Ps2=cell(1,f3mdp.n_states);
f3mdp.Da=cell(1,f3mdp.n_states);

f3mdp.Pa=cell(1,f3mdp.n_states);
f3mdp.Ra=cell(1,f3mdp.n_states);
for i=1:n
    f3mdp.Pa{i}=i;
    f3mdp.Ra{i}=i;
end

% Construction of Ploc
f3mdp.Ploc=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    n_v=length(f3mdp.Ps{i})+length(f3mdp.Ps2{i})+length(f3mdp.Pa{i});
    dim=zeros(1,n_v);
    dim(1)=f3mdp.S(i);
    cpt=2;
    for k=1:length(f3mdp.Ps{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ps2{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps2{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Pa{i})
        dim(cpt)=f3mdp.A(f3mdp.Pa{i}(k));
        cpt=cpt+1;
    end
    % [dim,1] : precaution for the case where dim is an integer (Ps,Ps2 and Pa
    % are empty)
    f3mdp.Ploc{i}=rand([dim,1]);
    f3mdp.Ploc{i}=normalize_mat(f3mdp.Ploc{i},1);
end


% Construction of Rloc
f3mdp.Rloc=cell(1,f3mdp.n_rewards);
for i=1:f3mdp.n_rewards
    n_v=length(f3mdp.Rs{i})+length(f3mdp.Ra{i});
    dim=zeros(1,n_v);
    cpt=1;
    for k=1:length(f3mdp.Rs{i})
        dim(cpt)=f3mdp.S(f3mdp.Rs{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ra{i})
        dim(cpt)=f3mdp.A(f3mdp.Ra{i}(k));
        cpt=cpt+1;
    end
    if(~isempty(dim)) % precaution for particular case
        f3mdp.Rloc{i}=squeeze(rand([dim,1])); 
    end
end

% Construction of P_0 : uniform probability
% according to the article on GMDPs
f3mdp.P_0=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    f3mdp.P_0{i}=1/f3mdp.S(i)*ones(f3mdp.S(i),1);
end


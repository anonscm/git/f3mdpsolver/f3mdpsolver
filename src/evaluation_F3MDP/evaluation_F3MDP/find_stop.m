function[res]=find_stop(t,it_max,gamma,eps,r_max,er_abs,V,h_f)    

% function that gives the answer to the stopping time test (based on the
% absolute error or the relative error in the case of an infinite horizon)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% t : current time
%% it_max : horizon
%% gamma : discount factor
%% eps : bound for the absolute or relative error
%% r_max : bound of the reward
%% er_abs : 1 if absolute error, relative error else
%% V : current value
%% h_f : 1 if finite horizon
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% res : boolean that indicates if we have to continue or not
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% FINITE HORIZON
if (h_f==1) 
    res=(t<=it_max);
    
%%%% INFINITE HORIZON 
else
  	phi=gamma^(t+1)/(1-gamma)*r_max; 
    
    if(er_abs) % ABSOLUTE ERROR
        res=(phi>eps && t<=it_max);
        
    else % RELATIVE ERROR
        res=((phi/V)>eps && t<=it_max);
    end
end

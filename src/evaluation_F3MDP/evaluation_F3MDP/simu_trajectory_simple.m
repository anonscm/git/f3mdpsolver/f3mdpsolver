function simu_trajectory_simple(f3mdp,Dloc,it_max)

% simulates a trajectory for a fiven policy
% without display of the value (see also function simu_trajectory)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the f3mdp
%% Dloc : the policy
%% it_max : horizon for the simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ploc=f3mdp.Ploc;
n_states=f3mdp.n_states;
n_actions=f3mdp.n_actions;

ordres=f3mdp.ordres;
ordrea=f3mdp.ordrea;

% if orders=[] we use the ascending order
if(isempty(ordres))
    ordres=1:n_states;
end
% if ordera=[] we use the ascending order
if(isempty(ordrea))
    ordrea=1:n_actions;
end

% current state anc action
s_t=zeros(1,n_states);
s_t_new=zeros(1,n_states);
a_t=zeros(1,n_actions);
    
t=0;

%s_0
for i=1:n_states
    % true : with replacement
    s_t(i)=randsample(f3mdp.S(i),1,true,f3mdp.P_0{i});
end
disp('Time 0')
disp('State :')
disp(s_t)

%a_0
for j=ordrea
    etat_pa=[s_t(f3mdp.Ds{j}),a_t(f3mdp.Da{j})];
    ind=num2cell(etat_pa);
    a_t(j)=randsample(f3mdp.A(j),1,true,Dloc{j}(:,ind{:}));
end
disp('Action :')
disp(a_t)


while(t<=it_max) 
   
    t=t+1;

    %s_t
    for j=ordres
        etat_pa=[s_t(f3mdp.Ps{j}),s_t_new(f3mdp.Ps2{j}),a_t(f3mdp.Pa{j})];
        ind=num2cell(etat_pa);
        s_t_new(j)=randsample(f3mdp.S(j),1,true,Ploc{j}(:,ind{:})); 
    end
    s_t=s_t_new; 
    disp('Time ')
    disp(t)
    disp('State :')
    disp(s_t)

    %a_t
    for j=ordrea
        etat_pa=[s_t(f3mdp.Ds{j}),a_t(f3mdp.Da{j})];
        ind=num2cell(etat_pa);
        a_t(j)=randsample(f3mdp.A(j),1,true,Dloc{j}(:,ind{:}));
    end
    disp('Action :')
    disp(a_t)
end




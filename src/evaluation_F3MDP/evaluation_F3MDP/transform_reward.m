function[Rnew,r_max,r_max_new]=transform_reward(R)

% transforms the (additive) rewards of an F^3MDP in order to transform the
% optimization problem into a minimization problem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% R : initial reward function (list of tables)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Rnew : new reward function (list of tables)
%% r_max : sum of the maxima of each reward function (old reward)
%% r_max_new : sum of the maxima of each reward function (new reward)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

k=length(R);

Rnew=cell(1,k);
r_max=0;
r_max_new=0;
for i=1:k
   mymax=max(R{i}(:));  
   r_max=r_max+mymax;
   Rnew{i}=mymax-R{i};
   mymaxnew=max(Rnew{i}(:));
   r_max_new=r_max_new+mymaxnew;
end

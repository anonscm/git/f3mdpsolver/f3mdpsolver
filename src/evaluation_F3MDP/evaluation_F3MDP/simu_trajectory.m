function simu_trajectory(f3mdp,Dloc,it_max,add,gamma)

% simulates a trajectory for a given policy
% with display of the value (see also function simu_trajectory_simple)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the f3mdp
%% Dloc : the policy
%% it_max : horizon for the simulation
%% add : 1 if additive rewards, else multiplicative
%% gamma : discount factor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ploc=f3mdp.Ploc;
Rloc=f3mdp.Rloc;
n_states=f3mdp.n_states;
n_actions=f3mdp.n_actions;
n_rewards=f3mdp.n_rewards;

orders=f3mdp.orders;
ordera=f3mdp.ordera;

% if orders=[] we use the ascending order
if(isempty(orders))
    orders=1:n_states;
end
% if ordera=[] we use the ascending order
if(isempty(ordera))
    ordera=1:n_actions;
end

% current state and actions
s_t=zeros(1,n_states);
s_t_new=zeros(1,n_states);
a_t=zeros(1,n_actions);
    
t=0;
V=0;

%s_0
for i=1:n_states
    % true : with replacement
    s_t(i)=randsample(f3mdp.S(i),1,true,f3mdp.P_0{i});
end
disp('Time 0')
disp('State :')
disp(s_t)

%a_0
for j=ordera
    etat_pa=[s_t(f3mdp.Ds{j}),a_t(f3mdp.Da{j})];
    ind=num2cell(etat_pa);
    a_t(j)=randsample(f3mdp.A(j),1,true,Dloc{j}(:,ind{:}));
end
disp('Action :')
disp(a_t)


while(t<=it_max) 
    if(add==1)
        V_cur=0;
    else
        V_cur=1;
    end
    for k=1:n_rewards
        etat_pa=[s_t(f3mdp.Rs{k}),a_t(f3mdp.Ra{k})];
        ind=num2cell(etat_pa);
        if(add==1)
            V_cur=V_cur+Rloc{k}(ind{:});
        else
            V_cur=V_cur*Rloc{k}(ind{:});
        end
    end

    V=V+gamma^t*V_cur;
    t=t+1;

    %s_t
    for j=orders
        etat_pa=[s_t(f3mdp.Ps{j}),s_t_new(f3mdp.Ps2{j}),a_t(f3mdp.Pa{j})];
        ind=num2cell(etat_pa);
        s_t_new(j)=randsample(f3mdp.S(j),1,true,Ploc{j}(:,ind{:})); 
    end
    s_t=s_t_new; 
    disp('Time ')
    disp(t)
    disp('State :')
    disp(s_t)

    %a_t
    for j=ordera
        etat_pa=[s_t(f3mdp.Ds{j}),a_t(f3mdp.Da{j})];
        ind=num2cell(etat_pa);
        a_t(j)=randsample(f3mdp.A(j),1,true,Dloc{j}(:,ind{:}));
    end
    disp('Action :')
    disp(a_t)
end

disp('Finale value :')
disp(V)



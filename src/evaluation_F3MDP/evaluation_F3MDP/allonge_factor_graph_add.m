function[fg2]=allonge_factor_graph_add(fg,f3mdp,Dloc,gmdp,stat)

% function that extends of one time slice the factor graph corresponding to
% a f3mdp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% fg : the factor graph before extension
%% f3mdp : the F^3MDP
%% Dloc : factored stochastic policy (stationary or not)
%% gmdp : 1 if the f3mdp is a gmdp (if Ds=Rs and Ra{i}=i)
%% stat : 1 if Dloc is stationary, 0 else
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% fg2 : the factor graph after extension
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT and OUTPUT : factor graph in LibDAI format
%% fg : list of length the number of factors
% fg{a}.Member = variables included in the scope of factor a
% fg{a}.P = table corresponding to factor a

% by default Dloc is supposed to be stationary
if(nargin<=3)
    stat=1;
end

n_states=f3mdp.n_states;
n_actions=f3mdp.n_actions;
n_rewards=f3mdp.n_rewards;

nb_f_old=numel(fg);

% preallocation
if(gmdp)
    fg2=cell(1,nb_f_old+n_states+n_actions);
else
    fg2=cell(1,nb_f_old+n_states+n_actions+n_rewards);
end
fg2(1:nb_f_old)=fg;

% current time slice
if(gmdp)
    t=nb_f_old/(n_states+n_actions);
else
    t=nb_f_old/(n_states+n_actions+n_rewards);
end

% begin : place where we will begin to rewrite
begin=nb_f_old+1;

% Add facors p (transition)
for k=begin:(begin+n_states-1)
    i=k-begin+1; % number of the state variable for the f3mdp
    fg2{k}.Member=[num2num(i,'s',t+1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Ps{i},'s',t,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Ps2{i},'s',t+1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Pa{i},'a',t,n_states,n_actions,n_rewards,gmdp)];
    fg2{k}.P=f3mdp.Ploc{i};
end

% Add factors delta (policy)
for j=(begin+n_states):(begin+n_states+n_actions-1)
    a=j-(begin+n_states)+1; % number of the action variable for the f3mdp
    fg2{j}.Member=[num2num(a,'a',t+1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Ds{a},'s',t+1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Da{a},'a',t+1,n_states,n_actions,n_rewards,gmdp)];
    if(stat) % Dloc stationary
        fg2{j}.P=Dloc{a};
    else
        fg2{j}.P=Dloc{t,a};
    end
end

% if not GMDP
% Add factors reward
% factors containing only 1 (in order to be able to obtain the beliefs)
if(~gmdp)
    for k=(begin+n_states+n_actions):(begin+n_states+n_actions+n_rewards-1)
        rec=k-(begin+n_states+n_actions)+1; % number of the reward function for the f3mdp
        fg2{k}.Member=[num2num(f3mdp.Rs{rec},'s',t+1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Ra{rec},'a',t+1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Ra2{rec},'a',t,n_states,n_actions,n_rewards,gmdp)];

        vect_dim=[f3mdp.S(f3mdp.Rs{rec}),f3mdp.A(f3mdp.Ra{rec}),f3mdp.A(f3mdp.Ra2{rec})];
        dims=num2cell(vect_dim);
        if(~isempty(dims)) % precaution for particular case
            fg2{k}.P=squeeze(ones([dims{:},1])); 
        else
            fg2{k}.P=[];
        end
    end
end



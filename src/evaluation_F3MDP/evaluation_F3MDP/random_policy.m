function[Dloc]=random_policy(f3mdp,stat,T)

% initializes the policy of an F^3MDP as random
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% f3mdp : the f3mdp
%% stat : =1 if we want a stationary policy, 0 else
%% T : horizon (useful if we want a non stationary policy), time will go
% from 0 to T
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Dloc : random factored policy (stationary or not)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% by default stationary policy
if(nargin<=1)
    stat=1;
end

% STATIONARY CASE
if(stat)
    % Construction of Dloc
    Dloc=cell(1,f3mdp.n_actions);
    for i=1:f3mdp.n_actions
            n_v=length(f3mdp.Ds{i})+length(f3mdp.Da{i});
            dim=zeros(1,n_v);
            dim(1)=f3mdp.A(i);
            cpt=2;
            for k=1:length(f3mdp.Ds{i})
                dim(cpt)=f3mdp.S(f3mdp.Ds{i}(k));
                cpt=cpt+1;
            end
            for k=1:length(f3mdp.Da{i})
                dim(cpt)=f3mdp.A(f3mdp.Da{i}(k));
                cpt=cpt+1;
            end 
            % [dim,1] : precaution for the case where dim is an integer (Ds and Da
            % are empty)
            Dloc{i}=rand([dim,1]);
            Dloc{i}=normalize_mat(Dloc{i},1);
    end
    
% NON STATIONARY CASE
else
    % Construction of Dloc
    Dloc=cell(T+1,f3mdp.n_actions);
    
    for i=1:f3mdp.n_actions
         n_v=length(f3mdp.Ds{i})+length(f3mdp.Da{i});
            dim=zeros(1,n_v);
            dim(1)=f3mdp.A(i);
            cpt=2;
            for k=1:length(f3mdp.Ds{i})
                dim(cpt)=f3mdp.S(f3mdp.Ds{i}(k));
                cpt=cpt+1;
            end
            for k=1:length(f3mdp.Da{i})
                dim(cpt)=f3mdp.A(f3mdp.Da{i}(k));
                cpt=cpt+1;
            end 
            
        for t=1:(T+1) % time 0 to T           
            % [dim,1] : precaution for the case where dim is an integer (Ds and Da
            % are empty)
            Dloc{t,i}=rand([dim,1]);
            Dloc{t,i}=normalize_mat(Dloc{t,i},1);
        end
    end
end

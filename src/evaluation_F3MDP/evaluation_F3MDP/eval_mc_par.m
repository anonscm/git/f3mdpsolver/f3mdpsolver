function[Vf,V,R]=eval_mc_par(f3mdp,Dloc,n_simu,gamma,h_f,eps,it_max,r_max,er_abs,add,orders,ordera)

% function that approximately evaluate a given policy Dloc of a given F^3MDP f3mdp with
% parallel Monte-Carlo simulations of trajectories
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the f3mdp
%% Dloc : factored stochastic policy to evaluate (stationary or not)
%% gamma : discount factor
%% h_f : 1 if finite horizon 
%% epsilon : theoretical bound on the absolute or relative error (case of
% the infinite horizon)
%% it_max : horizon (case of the finite or infinte horizon)
%% r_max : bound on the reward
%% er_abs : 1 if criterion based on the absolute error, else relative error
%% add : 1 if additive rewards, else multiplicative
%% orders : order on the state variables (useful when Ps2 is not
% empty), vector of size n_states
%% ordera : order on the action variables (useful when Da is not
% empty), vector of size n_actions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% Vf : value of the policy for initial distribution P_0
%% V : vector of the values of each trajectory
%% R : list of the rewards obtained at each time step for each trajectory
% R{i} : vector of the rewards obtained during trajectory i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ploc=f3mdp.Ploc;
Rloc=f3mdp.Rloc;
n_states=f3mdp.n_states;
n_actions=f3mdp.n_actions;
n_rewards=f3mdp.n_rewards;
Rs=f3mdp.Rs;
Ra=f3mdp.Ra;
Ra2=f3mdp.Ra2;
S=f3mdp.S;
A=f3mdp.A;
P_0=f3mdp.P_0;
Ds=f3mdp.Ds;
Da=f3mdp.Da;
Ps=f3mdp.Ps;
Pa=f3mdp.Pa;
Ps2=f3mdp.Ps2;

if(size(Dloc,1)==1) % Dloc stationary
    stat=1;
else
    stat=0; % Dloc non stationary
    % in this case, Dloc{t,i} represents the local policy associated with action variable i and time step t-1 
end


% default values for orders and ordera
if(nargin < 11)
    orders=[];
    ordera=[];
end

% if orders=[] we use the ascending order
if(isempty(orders))
    orders=1:n_states;
end
% if ordera=[] we use the ascending order
if(isempty(ordera))
    ordera=1:n_actions;
end

% R : list of the rewards obtained at each time step for each trajectory
R=cell(1,n_simu);

% V : vector of the values of each trajectory
V=zeros(1,n_simu);
parfor simu=1:n_simu
    %disp(['eval_mc : simulation ',num2str(simu)]);
    
    %%%%%%%%%%% Time 0 %%%%%%%%
    t=0;
    % current state and action
    s_t=zeros(1,n_states);
    s_t_new=zeros(1,n_states);
    a_t=zeros(1,n_actions);
  
    %s_0
    for i=1:n_states
        % true means with replacement
        s_t(i)=randsample(S(i),1,true,P_0{i});
    end
    
    %a_0
    for j=ordera
        etat_pa=[s_t(Ds{j}),a_t(Da{j})];
        ind=num2cell(etat_pa);
        if(stat)
            a_t(j)=randsample(A(j),1,true,Dloc{j}(:,ind{:}));
        else
            a_t(j)=randsample(A(j),1,true,Dloc{t+1,j}(:,ind{:}));
        end
    end
    
    if(add==1)
        V_cur=0;
    else
        V_cur=1;
    end
    for k=1:n_rewards
        etat_pa=[s_t(Rs{k}),a_t(Ra{k})];
        ind=num2cell(etat_pa);
        if(add==1)
            V_cur=V_cur+Rloc{k}(ind{:});
        else
            V_cur=V_cur*Rloc{k}(ind{:});
        end
    end
    a_t_old=a_t;
    R{simu}(1)=V_cur;

    V(simu)=V(simu)+V_cur;    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%% Time 1 %%%%%%%% 
    t=t+1;
  
    %s_1
    for j=orders
        etat_pa=[s_t(Ps{j}),s_t_new(Ps2{j}),a_t(Pa{j})];
        ind=num2cell(etat_pa);
        s_t_new(j)=randsample(S(j),1,true,Ploc{j}(:,ind{:})); 
    end
    s_t=s_t_new;
    
    %a_1
    for j=ordera
        etat_pa=[s_t(Ds{j}),a_t(Da{j})];
        ind=num2cell(etat_pa);
        if(stat)
            a_t(j)=randsample(A(j),1,true,Dloc{j}(:,ind{:}));
        else
            a_t(j)=randsample(A(j),1,true,Dloc{t+1,j}(:,ind{:}));
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%% Time >1 %%%%%%%%
    while(find_stop(t,it_max,gamma,eps,r_max,er_abs,V(simu),h_f)) 
        if(add==1)
            V_cur=0;
        else
            V_cur=1;
        end
        for k=1:n_rewards
            etat_pa=[s_t(Rs{k}),a_t(Ra{k}),a_t_old(Ra2{k})];
            ind=num2cell(etat_pa);
            if(add==1)
                V_cur=V_cur+Rloc{k}(ind{:});
            else
                V_cur=V_cur*Rloc{k}(ind{:});
            end
        end
        a_t_old=a_t;
        R{simu}(t+1)=V_cur;
        
        V(simu)=V(simu)+gamma^t*V_cur;
        t=t+1;
        
        if(find_stop(t,it_max,gamma,eps,r_max,er_abs,V(simu),h_f))
            %s_t
            for j=orders
                etat_pa=[s_t(Ps{j}),s_t_new(Ps2{j}),a_t(Pa{j})];
                ind=num2cell(etat_pa);
                s_t_new(j)=randsample(S(j),1,true,Ploc{j}(:,ind{:})); 
            end
            s_t=s_t_new; 

            %a_t
            for j=ordera
                etat_pa=[s_t(Ds{j}),a_t(Da{j})];
                ind=num2cell(etat_pa);
                if(stat)
                    a_t(j)=randsample(A(j),1,true,Dloc{j}(:,ind{:}));
                else
                    a_t(j)=randsample(A(j),1,true,Dloc{t+1,j}(:,ind{:}));
                end
            end
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%

end
Vf=mean(V);



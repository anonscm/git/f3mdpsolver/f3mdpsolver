function[num_fg]=num2num(num, type,t,n_states,n_actions,n_rewards,gmdp)

% function that takes as input the number of a state variable, action
% variable, or of a factor 'p', 'd' or 'r' and gives the associated number
% of the variable or factor in the factor graph
% (case of a GMDP : there are no reward factors in the factor graph)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% num : vector of the numbers of variables or factors in the F^3MDP
%% type : s (state), a (action), p (transition), d (policy) or r (reward)
%% t : considered time step
%% n_states, n_actions : number of state variables and action variables in
% the F^3MDP
%% n_rewards : number of reward functions in the F^3MDP
%% gmdp : 1 if the F^3MDP is a GMDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% num_fg : associated vector of the numbers of the variables or factors in the factor graph
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(t>=1)
    if(gmdp)
        % no need of reward factors in the factor graph
        switch type
            case 'r'
            disp('num2num : no rewards in the fg when GMDP');
            otherwise
            num_fg=num2num(num, type,t,n_states,n_actions);
         end
    
    else
        switch type
            case 's'
            num_fg=(n_states+n_actions)*(t-1)+num; 
            case 'a'
            num_fg=(n_states+n_actions)*(t-1)+n_states+num; 
            case 'p'
            num_fg=(n_states+n_actions+n_rewards)*(t-1)+num;
            case 'd'
            num_fg=(n_states+n_actions+n_rewards)*(t-1)+n_states+num;
            case 'r'
            num_fg=(n_states+n_actions+n_rewards)*(t-1)+n_states+n_actions+num;
            otherwise
            disp('num2num : incorrect type');
         end
      end
    
    
else
    error('num2num : t must be greater or equal to 1');
end

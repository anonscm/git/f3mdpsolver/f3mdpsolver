function[P,R,policy,P_0]=f3mdp_globalize_par(f3mdp,Dloc,add)

% function that transforms a small f3mdp into an MDP in the MDP toolbox
% format, and an associated factored stochastic policy into a global stochastic policy 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% Dloc : factored stochastic policy
%% add : 1 if additive rewards, else multiplicative
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT : MDP
%% P : P(s'|s,a) order of the dimensions : S^{t} then S^{t+1} then A^t
%% R : R(s,a) order of the dimensions : S^t then A^t
%% policy : pi(s,a), order of the dimensions : A^t then S^t
%% P_0 : column vector corresponding to the initial distribution on states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_states=f3mdp.n_states;
n_actions=f3mdp.n_actions;
n_rewards=f3mdp.n_rewards;

Prod_s=prod(f3mdp.S);  
Prod_a=prod(f3mdp.A);

if(add==1)
    R=zeros(Prod_s,Prod_a);
else
    R=ones(Prod_s,Prod_a);
end
P=ones(Prod_s,Prod_s,Prod_a);
policy=ones(Prod_a,Prod_s);

parfor s_t=1:Prod_s
    
    s_t_v=n_vec(s_t,f3mdp.S);
    for a_t=1:Prod_a
        a_t_v=n_vec(a_t,f3mdp.A);
        % R
        for k=1:n_rewards
            pa_s=f3mdp.Rs{k};
            pa_a=f3mdp.Ra{k};
            etat_pa=[s_t_v(pa_s),a_t_v(pa_a)];
            ind=num2cell(etat_pa);
            if(add==1)
                R(s_t,a_t)=R(s_t,a_t)+f3mdp.Rloc{k}(ind{:});
            else
                R(s_t,a_t)=R(s_t,a_t)*f3mdp.Rloc{k}(ind{:});
            end
        end
        % P
        for s_suiv=1:Prod_s
            s_suiv_v=n_vec(s_suiv,f3mdp.S);
            for i=1:n_states
                pa_s=f3mdp.Ps{i};
                pa_a=f3mdp.Pa{i};
                pa_s2=f3mdp.Ps2{i};
                etat_pa=[s_suiv_v(i),s_t_v(pa_s),s_suiv_v(pa_s2),a_t_v(pa_a)];
                ind=num2cell(etat_pa);
                P(s_t,s_suiv,a_t)=P(s_t,s_suiv,a_t)*f3mdp.Ploc{i}(ind{:});
            end
        end
        
        % policy
            for j=1:n_actions 
                pa_s=f3mdp.Ds{j};
                pa_a=f3mdp.Da{j};
                etat_pa=[a_t_v(j),s_t_v(pa_s),a_t_v(pa_a)];
                ind=num2cell(etat_pa);
                policy(a_t,s_t)=policy(a_t,s_t)*Dloc{j}(ind{:});
            end
    end
end    

P_0=success_kron_rev(f3mdp.P_0);






function[f]=eval_1D(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,add,r_max,fg,gmdp,policy_param,j,v_vec_list)

% evaluation function for coordinate_descent_fminbnd
% the optimization variable is policy_param
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP (with Ds and Da but not Dloc)
%% gamma : discount factor
%% h_f : 1 if finite horizon (but with discount), 0 if infinite horizon with
% discount
%% epsilon : theoretical bound on the absolute error (case of the infinite horizon) 
%% it_max : horizon (case of the infinite or finite horizon)
%% param : parameters for libDAI
%% add : 1 if additive rewards, else multiplicative rewards
%% r_max : sum of the maximum local rewards of the f3mdp
%% gmdp : 1 si the f3mdp is a gmdp
%% policy_param : parameter of the policy we want to optimize
%% j , v_vec_list : allow to locate the parameter in Dloc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% f : evaluation 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%% allocate the parameter in Dloc
Dloc{j}(1,v_vec_list{:})=policy_param;

%%%%%% evaluate
f=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp);

function[f]=eval_reparam_step(f3mdp,gamma,h_f,eps,it_max,meth,param,add,r_max,fg,gmdp,theta,grad,pas,Q)

% evaluation function used for gradient descent to optimize the step with
% fminbnd
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% gamma : discount factor
%% h_f : 1 if finite horizon (with discount), 0 if infinite horizon (with discount)
%% eps : theoretical bound on the absolute error (case of the infinite
% horizon)
%% it_max : horizon (case of the finite or infinite horizon)
%% meth : method for libDAI (ex : BP)
%% param : parameters for libDAI
%% add : 1 if additive rewards, else multiplicative
%% r_max : sum of the maxima local rewards of the F^3MDP
%% fg : factor graph
%% gmdp : 1 if the f3mdp is a gmdp
%% theta : current parameter
%% grad : gradient of theta
%% pas : step (that we want to optimize)
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUPUT
%% f : evaluation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

theta_new=theta-pas*grad/norm(grad);

Dloc=ThetaToDloc(f3mdp,theta_new,Q);

%%%%%% evaluate
f=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp);

function[V,T]=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp)

% function that approximately evaluates a given policy Dloc of a given F^3MDP f3mdp
% based on computation of beliefs with libDAI library
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the f3mdp
%% Dloc : factored stochastic policy to evaluate (stationary or not)
%% gamma : discount factor
%% h_f : 1 if finite horizon (with discount), 0 if infinite horizon (with discount)
%% eps : theoretical bound on the absolute error (case of the infinite horizon)
%% it_max : horizon (case of the finite or infinite horizon)
%% meth : method for libDAI (ex : 'BP')
%% param : parameters for libDAI
%%% by default use :
%%% param='[updates=SEQFIX,tol=1e-1,maxiter=1000,logdomain=1,inference=SUMPROD,damping=0,verbose=0]';
%%% if there are numerical problems, choose logdomain=0
%% r_max : bound of the reward (to compute the stopping time in the case of
% an infinite horizon)
%% add : 1 if additive rewards, else multiplicative
%% fg : factor graph 
%% gmdp : 1 if the f3mdp is a gmdp (if Ds=Rs and Ra{i}=i, Ra2 empty)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% V : value of the policy for the initial distribution P_0
%% T : horizon used
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% path for libDAI library
addpath('/usr/local/MATLAB/current/libDAI-0.3.1/matlab');

if(size(Dloc,1)==1) % Dloc stationary
    stat=1;
else
    stat=0; % Dloc non stationary
    % in this case, Dloc{t,i} represents the local policy associated with action variable i at time t-1 
end

% verification
if(add~=1)
   error('evaluation : Caution!!! This evaluation method works only for additive rewards.'); 
end

% stopping time
if(h_f==1)
    T=it_max;
else % infinite horizon : we have to work with absolute error criterion in order to know in advance the stopping time
    T=arret(gamma,eps,r_max);
end

% load the policy into the factor graph
for i=1:f3mdp.n_actions
    for t=1:(T+1) % time 0 to T
        mynum=num2num(i,'d',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp);
        if(stat) % Dloc stationary
            fg{mynum}.P=Dloc{i};
        else
            fg{mynum}.P=Dloc{t,i};
        end
    end
end

% Inference
if(~strcmp('CBP',meth))
    [~,~,~,~,qf] = dai (fg, meth, param);
else % CBP method
    % for this method, we cannot obtain only qf, we need to get back all q
    [~,q,~,~] = dai (fg, meth, param);
    qf=q((length(q)-length(fg)+1):length(q));
end

% Compute V from the beliefs
% caution because libDAI gives the beliefs in a different order than the
% factor's member vector (it gives them in ascending order)

% Case of a GMDP (Ds=Rs)
if(gmdp)
    V=0;
    for k=1:f3mdp.n_rewards
        V2=0;
       for t=1:(T+1) % time 0 to T
           num_fg=num2num(k,'d',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp);
           V2=V2+gamma^(t-1)*qf{num_fg}.P;
       end
       % the order of the b_k^t in .Member is the same for all times t
       vect=[num2num(f3mdp.Rs{k},'s',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp),num2num(f3mdp.Ra{k},'a',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp),num2num(f3mdp.Ra2{k},'a',t-1,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp)];
       % the line under allows to find the indexes of the values in vector
       % vect in the vector qf{num_fg}.Member  
       dims = sort(arrayfun(@(x)find(qf{num_fg}.Member==x,1),vect));
       M=permute(V2,dims).*f3mdp.Rloc{k};
       V=V+sum(M(:));
    end

% Case of a non PDMG
else
    
    V=0;
    for k=1:f3mdp.n_rewards
        V2=0;
       for t=1:(T+1) % time 0 to T
           num_fg=num2num(k,'r',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp);
           %V2=V2+gamma^(t-1)*qf{num_fg}.P;
                      
           if(t>1)
               vect=[num2num(f3mdp.Rs{k},'s',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp),num2num(f3mdp.Ra{k},'a',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp),num2num(f3mdp.Ra2{k},'a',t-1,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp)];
           else
               vect=[num2num(f3mdp.Rs{k},'s',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp),num2num(f3mdp.Ra{k},'a',t,f3mdp.n_states,f3mdp.n_actions,f3mdp.n_rewards,gmdp)];
           end
           % precaution for the case in which f3mdp.Ra2 is non empty, the
           % belief of the first time slice has a lower dimension because
           % there are no actions from the previous time step
           % the line under allows to find the indexes of the values in vector
            % vect in the vector qf{num_fg}.Member  
           dims = arrayfun(@(x)find(qf{num_fg}.Member==x,1),vect);
           if(length(dims)>1)
               V2=bsxfun(@plus,V2,permute(gamma^(t-1)*qf{num_fg}.P,dims));
           else
               V2=bsxfun(@plus,V2,gamma^(t-1)*qf{num_fg}.P);
           end
       end
                  
        M=V2.*f3mdp.Rloc{k};
        V=V+sum(M(:));
    end
end




    
    

function[fg]=initialize_factor_graph_add(f3mdp,Dloc,gmdp)

% function that initializes the factor graph corresponding to a f3mdp
% builds the first time slice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% Dloc : factored stochastic policy (stationary or not)
%% gmdp : 1 if the f3mdp is a gmdp (if Ds=Rs and Ra{i}=i)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% fg : factor graph in libDAI format (list of size the number of factors)
% fg{a}.Member = variables included in the scope of factor a
% fg{a}.P = table corresponding to factor a
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


n_states=f3mdp.n_states;
n_actions=f3mdp.n_actions;
n_rewards=f3mdp.n_rewards;

n_x=n_states+n_actions;
fg=cell(1,n_x);

% slice 1 (time 0)
for i=1:n_states
    % factors p (transition)
    fg{i}.Member=num2num(i,'s',1,n_states,n_actions,n_rewards,gmdp);
    fg{i}.P=f3mdp.P_0{i};
end
for j=1:n_actions
    % factors delta (policy)
    fg{n_states+j}.Member=[num2num(j,'a',1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Ds{j},'s',1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Da{j},'a',1,n_states,n_actions,n_rewards,gmdp)];
    fg{n_states+j}.P=Dloc{1,j}; % Dloc{1,j} -> marche que Dloc soit stationnaire ou non stationnaire
end

% if not PDMG
% for rewards, we put rewards containing only 1
if(~gmdp)
    for k=1:n_rewards
        fg{n_states+n_actions+k}.Member=[num2num(f3mdp.Rs{k},'s',1,n_states,n_actions,n_rewards,gmdp),num2num(f3mdp.Ra{k},'a',1,n_states,n_actions,n_rewards,gmdp)];
        vect_dim=[f3mdp.S(f3mdp.Rs{k}),f3mdp.A(f3mdp.Ra{k})];
        dims=num2cell(vect_dim);
        if(~isempty(dims)) % precaution for particular case
            fg{n_states+n_actions+k}.P=squeeze(ones([dims{:},1])); 
        else
            fg{n_states+n_actions+k}.P=[];
        end
    end
end





function[fg]=create_factor_graph(f3mdp,Dloc,gmdp,it_max)

% function that creates the factor graph associated to an F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP 
%% Dloc : factored stochastic policy (stationary or not)
%% gmdp : 1 if the f3mdp is a gmdp (if Ds=Rs and Ra{i}=i)
%% it_max : horizon
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% fg : the factor graph in libDAI format
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(size(Dloc,1)==1) % Dloc stationary
    stat=1;
else
    stat=0; % Dloc non stationary
    % in this case, Dloc{t,i} represents the local policy associated to action variable i at time t-1 
end

fg=initialize_factor_graph_add(f3mdp,Dloc,gmdp);
for t=1:it_max 
    fg=allonge_factor_graph_add(fg,f3mdp,Dloc,gmdp,stat);
end

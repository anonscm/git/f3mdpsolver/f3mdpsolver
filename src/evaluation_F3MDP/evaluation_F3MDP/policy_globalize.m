function[policy]=policy_globalize(f3mdp,Dloc)

% function that transforms the policy of a small F^3MDP into a global
% policy 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% Dloc : the policy to globalize
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% policy : pi(s,a), order of the dimensions : A^t then S^t
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_actions=f3mdp.n_actions;

Prod_s=prod(f3mdp.S);  
Prod_a=prod(f3mdp.A);

policy=ones(Prod_a,Prod_s);

for s_t=1:Prod_s

s_t_v=n_vec(s_t,f3mdp.S);
    for a_t=1:Prod_a
    a_t_v=n_vec(a_t,f3mdp.A);

    % policy
        for j=1:n_actions 
            pa_s=f3mdp.Ds{j};
            pa_a=f3mdp.Da{j};
            etat_pa=[a_t_v(j),s_t_v(pa_s),a_t_v(pa_a)];
            ind=num2cell(etat_pa);
            policy(a_t,s_t)=policy(a_t,s_t)*Dloc{j}(ind{:});
        end
    end
end    



function[isequal,j_a]=compa_policy(Dloc1,Dloc2,eps)

% functions that compares two policies
% (for example to compare to the greedy policy)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% Dloc1 and Dloc2 : the two policies
%% eps : difference authorized between the two policies (ex : 10^(-2))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% isequal : 1 if the two policies are equal, 0 else
%% j_a : index of the action variable for which there are differences
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

n_actions=length(Dloc1);
isequal=1;
j_a=0;

if(length(Dloc1)~=length(Dloc2))
    disp('compa_policy : Dloc1 and Dloc2 have not the same length, therefore they cannot be equal.');    
end

for j=1:n_actions
    v=(abs(Dloc1{j}-Dloc2{j})<=eps);
    if(sum(v(:))~=numel(Dloc1{j}))
        isequal=0;  
        j_a=j;
        break;
    end
end
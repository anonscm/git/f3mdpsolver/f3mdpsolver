function[f3mdp,Dloc]=rand_f3mdp_tables(f3mdp)

% generate an F^3MDP whose dependencies are known but tables are random
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%%  f3mdp : the F^3MDP
%%  Dloc  = local stationary policy, list of length n.actions,
%                  Dloc(j) =  matrix of size |A(j)|x prod_{i dans Ds(j)}
%                  |S(i)|x prod_{j2 dans Da(j)} |A(j2)|
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Construction of P_0 
f3mdp.P_0=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    f3mdp.P_0{i}=rand(f3mdp.S(i),1);
    f3mdp.P_0{i}=normalize_mat(f3mdp.P_0{i},1);
end

% Construction of Ploc
f3mdp.Ploc=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    n_v=length(f3mdp.Ps{i})+length(f3mdp.Ps2{i})+length(f3mdp.Pa{i});
    dim=zeros(1,n_v);
    dim(1)=f3mdp.S(i);
    cpt=2;
    for k=1:length(f3mdp.Ps{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ps2{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps2{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Pa{i})
        dim(cpt)=f3mdp.A(f3mdp.Pa{i}(k));
        cpt=cpt+1;
    end
    % [dim,1] : precaution for the case where dim is an integer (Ps,Ps2 and Pa
    % are empty)
    f3mdp.Ploc{i}=rand([dim,1]);
    f3mdp.Ploc{i}=normalize_mat(f3mdp.Ploc{i},1);
end

% Construction de Dloc
Dloc=cell(1,f3mdp.n_actions);
for i=1:f3mdp.n_actions
        n_v=length(f3mdp.Ds{i})+length(f3mdp.Da{i});
        dim=zeros(1,n_v);
        dim(1)=f3mdp.A(i);
        cpt=2;
        for k=1:length(f3mdp.Ds{i})
            dim(cpt)=f3mdp.S(f3mdp.Ds{i}(k));
            cpt=cpt+1;
        end
        for k=1:length(f3mdp.Da{i})
            dim(cpt)=f3mdp.A(f3mdp.Da{i}(k));
            cpt=cpt+1;
        end 
        % [dim,1] : precaution for the case where dim is an integer (Ds and Da
        % are empty)
        Dloc{i}=rand([dim,1]);
        Dloc{i}=normalize_mat(Dloc{i},1);
end

% Construction of Rloc
f3mdp.Rloc=cell(1,f3mdp.n_rewards);
for i=1:f3mdp.n_rewards
    n_v=length(f3mdp.Rs{i})+length(f3mdp.Ra{i});
    dim=zeros(1,n_v);
    cpt=1;
    for k=1:length(f3mdp.Rs{i})
        dim(cpt)=f3mdp.S(f3mdp.Rs{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ra{i})
        dim(cpt)=f3mdp.A(f3mdp.Ra{i}(k));
        cpt=cpt+1;
    end
    if(~isempty(dim)) % precaution for particular case
        f3mdp.Rloc{i}=squeeze(rand([dim,1])); 
    end
end


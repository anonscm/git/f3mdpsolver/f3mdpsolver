function[Dloc_det]=determinisation(f3mdp,Dloc)

% function that allows to transform a stochastic policy into the nearest
% deterministic policy
% particularly useful for big problems (impossible to globalize)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% Dloc : the stochastic factored policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Dloc_det : the deterministic factored policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Dloc_det=cell(1,f3mdp.n_actions);
for i=1:f3mdp.n_actions
	Dloc_det{i}=zeros(size(Dloc{i}));
	dim=[f3mdp.S(f3mdp.Ds{i}),f3mdp.A(f3mdp.Da{i})];
	for j=1:prod(dim)
		vect=n_vec(j,dim);
        vect2=num2cell(vect);
		[~,k]=max(Dloc{i}(:,vect2{:}));
		Dloc_det{i}(k,vect2{:})=1;
	end
end


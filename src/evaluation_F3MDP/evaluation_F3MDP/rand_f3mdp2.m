function[f3mdp,Dloc]=rand_f3mdp2(n_states,n_actions,n_rewards,n_max_s,n_max_a,n_max_pa,arc_synch)

% generate a random F^3MDP
% Ra2 empty (the reward doesn't depend on actions of the previous time
% step)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for MDPs (n_states=n_actions=n_rewards=1), use function rand_mdp instead
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% in this function, the number of parents and the size of the variables are
% precisely the ones given as inputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% n_states : number of state variables
%% n_actions : number of action variables
%% n_rewards : number of reward functions
%% n_max_s : number of possible states
%% n_max_a : number of possible actions
%% n_max_pa : number of neighbours for the factors in the factor graph
%% arc_synch : 1 synchronous arcs are authorized
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%%  f3mdp : the F^3MDP
%%  Dloc  = local stationary policy, list of length n.actions,
%                  Dloc(j) =  matrix of size |A(j)|x prod_{i dans Ds(j)}
%                  |S(i)|x prod_{j2 dans Da(j)} |A(j2)|
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


f3mdp.n_states=n_states;
f3mdp.n_actions=n_actions;
f3mdp.n_rewards=n_rewards;

% randi : with replacement
% randperm : without replacement

if (n_states==1 && n_actions==1) % case of an MDP
    f3mdp.S=n_max_s;
    f3mdp.A=n_max_a;
else
    f3mdp.S=n_max_s*ones(1,n_states);
    f3mdp.A=n_max_a*ones(1,n_actions); 
end

f3mdp.Ps=cell(1,f3mdp.n_states);
if(arc_synch)
    nb_par_Ps=randi([0,min(round((n_max_pa-1)/3),n_states)],[1,n_states]);
else
    nb_par_Ps=randi([0,min(round((n_max_pa-1)/2),n_states)],[1,n_states]);
end
for i=1:n_states
    f3mdp.Ps{i}=randperm(n_states,nb_par_Ps(i));
    f3mdp.Ps{i}=sort(f3mdp.Ps{i});
end

if(arc_synch)
    f3mdp.orders=1:n_states; % easier to use randperm after
    nb_par_Ps2=zeros(1,n_states);
    for i=1:n_states
        nb_par_Ps2(i)=randi([0,min(floor((n_max_pa-1)/3),i-1)]);
        f3mdp.Ps2{i}=randperm(i-1,nb_par_Ps2(i));
    end
else
    f3mdp.Ps2=cell(1,f3mdp.n_states);
end


f3mdp.Pa=cell(1,f3mdp.n_states);
if(arc_synch)
    nb_par_Pa=randi([0,min(floor((n_max_pa-1)/3),n_actions)],[1,n_states]);
else
    nb_par_Pa=randi([0,min(floor((n_max_pa-1)/2),n_actions)],[1,n_states]);
end
for i=1:n_states
    f3mdp.Pa{i}=randperm(n_actions,nb_par_Pa(i));
    f3mdp.Pa{i}=sort(f3mdp.Pa{i});
end

f3mdp.Ds=cell(1,f3mdp.n_actions);
if(arc_synch)
    nb_par_Ds=randi([0,min((n_max_pa-1)/2,n_states)],[1,n_actions]);
else
    nb_par_Ds=randi([0,min(n_max_pa-1,n_states)],[1,n_actions]);
end
for i=1:n_actions
    f3mdp.Ds{i}=randperm(n_states,nb_par_Ds(i));
    f3mdp.Ds{i}=sort(f3mdp.Ds{i});
end

if(arc_synch)
    f3mdp.ordera=1:n_actions; % easier to use randperm after
    nb_par_Da=zeros(1,n_actions);
    for i=1:n_actions
        nb_par_Da(i)=randi([0,min(floor((n_max_pa-1)/2),i-1)]);
        f3mdp.Da{i}=randperm(i-1,nb_par_Da(i));
    end
else
    f3mdp.Da=cell(1,f3mdp.n_actions);
end

f3mdp.Rs=cell(1,f3mdp.n_rewards);
nb_par_Rs=randi([1,min(round(n_max_pa/2),n_states)],[1,n_rewards]);
for i=1:n_rewards
    f3mdp.Rs{i}=randperm(n_states,nb_par_Rs(i));
    f3mdp.Rs{i}=sort(f3mdp.Rs{i});
end

f3mdp.Ra=cell(1,f3mdp.n_rewards);
nb_par_Ra=randi([1,min(floor(n_max_pa/2),n_actions)],[1,n_rewards]); 

for i=1:n_rewards
    f3mdp.Ra{i}=randperm(n_actions,nb_par_Ra(i));
    f3mdp.Ra{i}=sort(f3mdp.Ra{i});
end

% Construction of P_0
f3mdp.P_0=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    f3mdp.P_0{i}=rand(f3mdp.S(i),1);
    f3mdp.P_0{i}=normalize_mat(f3mdp.P_0{i},1);
end

% Construction of Ploc
f3mdp.Ploc=cell(1,f3mdp.n_states);
for i=1:f3mdp.n_states
    n_v=length(f3mdp.Ps{i})+length(f3mdp.Ps2{i})+length(f3mdp.Pa{i});
    dim=zeros(1,n_v);
    dim(1)=f3mdp.S(i);
    cpt=2;
    for k=1:length(f3mdp.Ps{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ps2{i})
        dim(cpt)=f3mdp.S(f3mdp.Ps2{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Pa{i})
        dim(cpt)=f3mdp.A(f3mdp.Pa{i}(k));
        cpt=cpt+1;
    end
    % [dim,1] : precaution for the case where dim is an integer (Ps,Ps2 and Pa
    % are empty)
    f3mdp.Ploc{i}=rand([dim,1]);
    f3mdp.Ploc{i}=normalize_mat(f3mdp.Ploc{i},1);
end

% Construction de Dloc
Dloc=cell(1,f3mdp.n_actions);
for i=1:f3mdp.n_actions
        n_v=length(f3mdp.Ds{i})+length(f3mdp.Da{i});
        dim=zeros(1,n_v);
        dim(1)=f3mdp.A(i);
        cpt=2;
        for k=1:length(f3mdp.Ds{i})
            dim(cpt)=f3mdp.S(f3mdp.Ds{i}(k));
            cpt=cpt+1;
        end
        for k=1:length(f3mdp.Da{i})
            dim(cpt)=f3mdp.A(f3mdp.Da{i}(k));
            cpt=cpt+1;
        end
    % [dim,1] : precaution for the case where dim is an integer (Ds and Da
    % are empty)
        Dloc{i}=rand([dim,1]);
        Dloc{i}=normalize_mat(Dloc{i},1);
end

% Construction of Rloc
f3mdp.Rloc=cell(1,f3mdp.n_rewards);
for i=1:f3mdp.n_rewards
    n_v=length(f3mdp.Rs{i})+length(f3mdp.Ra{i});
    dim=zeros(1,n_v);
    cpt=1;
    for k=1:length(f3mdp.Rs{i})
        dim(cpt)=f3mdp.S(f3mdp.Rs{i}(k));
        cpt=cpt+1;
    end
    for k=1:length(f3mdp.Ra{i})
        dim(cpt)=f3mdp.A(f3mdp.Ra{i}(k));
        cpt=cpt+1;
    end
    if(~isempty(dim)) % precaution for particular case
        f3mdp.Rloc{i}=squeeze(rand([dim,1])); 
    end
end


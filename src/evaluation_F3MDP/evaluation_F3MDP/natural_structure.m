function[Ds,Da]=natural_structure(f3mdp)

% computes the natural structure for an F^3MDP without synchronous arcs
% we take the state variables that appear in the same reward functions as
% a_k
% and the state variables that influence the same state variables as a_k
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for a GMDP we have Ds=Ps=Rs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Ds : policy structure
%% Da : empty
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% compute Pa_inv
Pa_inv=cell(1,f3mdp.n_states);

for i=1:length(f3mdp.Pa)
    for j=f3mdp.Pa{i}
       Pa_inv{j}=[Pa_inv{j},i];
    end
end

% compute Ra_inv
Ra_inv=cell(1,f3mdp.n_states);

for i=1:length(f3mdp.Ra)
    for j=f3mdp.Ra{i}
       Ra_inv{j}=[Ra_inv{j},i];
    end
end

% Da empty
Da=cell(1,f3mdp.n_actions);

% Ds
Ds=cell(1,f3mdp.n_actions);
for a=1:f3mdp.n_actions
    Ds{a}=[f3mdp.Ps{Pa_inv{a}},f3mdp.Rs{Ra_inv{a}}];
   
    % take off the duplications and sort
    Ds{a}=unique(Ds{a});
end
% fonction qui transforme un pdmfaf (de petite taille) en pdm utilisable
% par la MDP toolbox

% utiliser plutot la version 1 de la parallelisation

% ENTREES
% pdmfaf : le PDMF-AF
% add : 1 si recompenses additives, sinon recompenses multiplicatives

% SORTIES : PDM
% P : P(s'|s,a) ordre des dimensions : S^{t} puis S^{t+1} puis A^t
% R : R(s,a) ordre des dimensions : S^t puis A^t
% policy : pi(s,a), ordre des dimensions : A^t puis S^t
% P_0 : vecteur colonne correspondant a la distribution sur l'etat initial

function[P,R,policy,P_0]=pdmfaf_globalize_par3(pdmfaf,add)

n_states=pdmfaf.n_states;
n_actions=pdmfaf.n_actions;
n_rewards=pdmfaf.n_rewards;

Prod_s=prod(pdmfaf.S);  
Prod_a=prod(pdmfaf.A);

if(add==1)
    R=zeros(Prod_s,Prod_a);
else
    R=ones(Prod_s,Prod_a);
end
P=ones(Prod_s,Prod_s,Prod_a);
policy=ones(Prod_a,Prod_s);

% a la place de n_vec je pourrais utiliser ind2sub de la manière suivante (pour recuperer toutes les sorties dans un vecteur) :
% [X{:}]=ind2sub(arguments)
for s_t=1:Prod_s
    
    s_t_v=n_vec(s_t,pdmfaf.S);
    for a_t=1:Prod_a
        a_t_v=n_vec(a_t,pdmfaf.A);
        % R
        for k=1:n_rewards
            pa_s=pdmfaf.Rs{k};
            pa_a=pdmfaf.Ra{k};
            etat_pa=[s_t_v(pa_s),a_t_v(pa_a)];
            ind=num2cell(etat_pa);
            if(add==1)
                R(s_t,a_t)=R(s_t,a_t)+pdmfaf.Rloc{k}(ind{:});
            else
                R(s_t,a_t)=R(s_t,a_t)*pdmfaf.Rloc{k}(ind{:});
            end
        end
        % P
        parfor s_suiv=1:Prod_s
            s_suiv_v=n_vec(s_suiv,pdmfaf.S);
            for i=1:n_states
                pa_s=pdmfaf.Ps{i};
                pa_a=pdmfaf.Pa{i};
                pa_s2=pdmfaf.Ps2{i};
                etat_pa=[s_suiv_v(i),s_t_v(pa_s),s_suiv_v(pa_s2),a_t_v(pa_a)];
                ind=num2cell(etat_pa);
                P(s_t,s_suiv,a_t)=P(s_t,s_suiv,a_t)*pdmfaf.Ploc{i}(ind{:});
            end
        end
        
    % policy
        for j=1:n_actions 
            pa_s=pdmfaf.Ds{j};
            pa_a=pdmfaf.Da{j};
            etat_pa=[a_t_v(j),s_t_v(pa_s),a_t_v(pa_a)];
            ind=num2cell(etat_pa);
            policy(a_t,s_t)=policy(a_t,s_t)*pdmfaf.Dloc{j}(ind{:});
        end
    end
end    

P_0=success_kron_rev(pdmfaf.P_0);

% On complète les politiques locales 
% pol_loc=pdmfaf.Dloc;
% for j=1:n_actions
%     pa_s=pdmfaf.Ds{j};
%     pa_a=pdmfaf.Da{j};
%     miss_s=setdiff(1:n_states,pa_s); 
%     miss_a=setdiff(1:n_actions,pa_a); 
%     for ms=miss_s
%        old_dim=size(pol_loc{j});
%        dim=[old_dim(1:ms-1),1,old_dim(ms:end)];
%        pol_loc{j}=reshape(pol_loc{j},dim);
%        temp=pol_loc{j}; 
%        for state=1:(pdmfaf.S(ms)-1)           
%            pol_loc{j}=cat(ms,temp,pol_loc{j});
%        end
%     end
%     for ma=miss_a
%        ind_a=ma+n_states; % on décale 
%        old_dim=size(pol_loc{j});
%        dim=[old_dim(1:ind_a-1),1,old_dim(ind_a:end)];
%        pol_loc{j}=reshape(pol_loc{j},dim);
%        temp=pol_loc{j}; 
%        for action=1:(pdmfaf.A(ma)-1)           
%            pol_loc{j}=cat(ind_a,temp,pol_loc{j});
%        end
%     end
% end
% 
% % On fait le produit
% policy=success_kron_rev(pol_loc);





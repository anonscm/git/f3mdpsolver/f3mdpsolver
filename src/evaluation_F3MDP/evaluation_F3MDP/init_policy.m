function[Dloc]=init_policy(f3mdp)

% function that allow to initialize a policy with uniform probability
% for initial point of the optimization algorithms
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Dloc : uniform factored policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Dloc=cell(1,f3mdp.n_actions);
for j=1:f3mdp.n_actions
    % the 1 at the end is a precaution for the case where Ds and Da are empty
    Dloc{j}=ones([f3mdp.A(j),f3mdp.S(f3mdp.Ds{j}),f3mdp.A(f3mdp.Da{j}),1]);
    Dloc{j}=normalize_mat(Dloc{j},1);
end
function [n]=vec_n(Y,sizeY)

% function from the GMDP toolbox that transforms a vector of local states
% into a global state
% equivalent to sub2ind
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% Y : vector of local states
%% sizeY : vector of the sizes of the local state spaces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% n : global state
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Transforms vector Y (1xndims(Y)) into a number n,
% knowing each coordinate i maximum sizeY(i).
 
Yprod = [1,cumprod(sizeY(1:size(Y,2)-1))];
n = (Y-1)*Yprod'+1;


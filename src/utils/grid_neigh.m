function[neigh]=grid_neigh(n,mysort)

% function that gives the neighbourhoods for a grid nxn in the case where
% we consider five neighbours (the cell itself, the cell above, the cell on
% the left, the cell on the right and the cell below)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the grid is supposed to be index in line
% example, grid 3x3 :
% 123
% 456
% 789
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% n : size of the grid
%% mysort : 1 if the neighbours have to be sorted by ascending order of index (default value of mysort : 1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% neigh : neighbourhood (list of vectors, the list has length n^2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin<=1)
   mysort=1; 
end

% number of variables
n_x=n^2;

neigh=cell(1,n_x);

% edges
bord_gauche=1:n:(n^2-(n-1));
bord_droit=n:n:n^2;
bord_haut=1:n;
bord_bas=(n^2-(n-1)):n^2;

for i=1:n_x
    % if i is on the left edge, no neighbour on the left
   if(ismember(i,bord_gauche)) 
       vg=[];
   else
       vg=i-1;
   end
    % if i is on the above edge, no neighbour above
   if(ismember(i,bord_haut)) 
       vh=[];
   else
       vh=i-n;
   end
    % if i is on the below edge, no neighbour below
   if(ismember(i,bord_bas)) 
       vb=[];
   else
       vb=i+n;
   end
    % if i is on the right edge, no neighbour on the right
   if(ismember(i,bord_droit)) 
       vd=[];
   else
       vd=i+1;
   end
   
   % neighbourhood of cell i
   neigh{i}=[i,vh,vg,vd,vb]; 
   if(mysort)
    neigh{i}=sort(neigh{i});
   end
end
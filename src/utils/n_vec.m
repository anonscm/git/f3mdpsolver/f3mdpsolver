function [Y]=n_vec(n,sizeY)

% function from the GMDP toolbox that transforms a global state into a
% vector of local states
% equivalent to ind2sub but the advantage is that outputs are grouped
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% n : global state
%% sizeY : vector of the sizes of the local state spaces
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Y : vector of local states
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXAMPLE
% n_vec(2,[2,2,2])=[2,1,1]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Transforms integer n<prod(sizeY)+1 into a
% vector Y such that ndims(Y)=size(sizeY).
  
if n==0 
  Y = 0;
else
  s = size(sizeY,2);
  Yprod = [1,cumprod(sizeY(1:s-1))];
  Y = zeros(1,s);
  m = n-1;
   
  for i = s:-1:1
    v2 = floor(m/Yprod(i));
    Y(1,i) = v2(:)';
    % positive rest of the division of m by Yprod(i)
    % m = pmodulo(m,Yprod(i));
    m=m - Yprod(i)*floor(m/Yprod(i));
  end
   
  Y = Y+1;
end
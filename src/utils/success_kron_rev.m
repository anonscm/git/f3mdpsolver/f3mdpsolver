function [res]=success_kron_rev(list)

% function that computes the tensoral product of several vectors on the
% left and gives a vector as output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% list : list of the column vectors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% res : result vector
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(list)==1
    res=list{1};
    
else

    res=list{1};
    for k=2:numel(list)
       v=list{k};
       res=kron(v,res);
    end
    
end
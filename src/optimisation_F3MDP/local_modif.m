function[Dloc_new]=local_modif(Dloc,cpt,eps,Q)

% for gradient descent
% allows to change the coordinates of the policy corresponding to a
% modification of one coordinate of theta : theta(cpt)=theta(cpt)+eps
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dloc=exp(theta)/sum(exp(theta))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% Dloc : old policy
%% cpt : index of the coordinate of theta we want to add eps
%% eps : epsilon to add
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% representing the equality constraints on the parameters of the policy
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Dloc_new : new policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(isempty(Q))
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % transformation of cpt into indexes (num_action,etat_var,etats_vois)
    % theta(cpt) <-> Dloc{num_action}(etat_var,etats_vois_list{:})

    n_actions=length(Dloc);

    % vector of the cumulated sizes of the blocks
    vect_tailles=zeros(1,n_actions);
    for i=1:n_actions
       vect_tailles(i)=vect_tailles(i)+numel(Dloc{i});
       if ((i+1)<=n_actions)
        vect_tailles(i+1)=vect_tailles(i);
       end
    end
    num_action=find(cpt<=vect_tailles,1);

    if(num_action>1)
        cpt_new=cpt-vect_tailles(num_action-1);
    else
        cpt_new=cpt;
    end

    vect_indices=n_vec(cpt_new,size(Dloc{num_action}));

    etat_var=vect_indices(1);
    etats_vois=vect_indices(2:end);
    etats_vois_list=num2cell(etats_vois);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    Dloc_new=Dloc;

    %%%% change in Dloc due to theta(cpt)<-theta(cpt)+eps_df
    % modification of the coordinate corresponding to cpt
    dk=Dloc{num_action}(etat_var,etats_vois_list{:});
    dk=(dk*exp(eps))/(1+(exp(eps)-1)*dk);
    Dloc_new{num_action}(etat_var,etats_vois_list{:})=dk;

    % modification of the other coordinates of the policy that need to be
    % changed
    long=length(Dloc{num_action}(:,etats_vois_list{:}));
    % scan all possible values for the action variable except etat_var
    vect=[1:etat_var-1,etat_var+1:long];
    for i=vect
        dg=Dloc{num_action}(i,etats_vois_list{:});
        dg=dg/(1+(exp(eps)-1)*dk);
        Dloc_new{num_action}(i,etats_vois_list{:})=dg;
    end
    %%%%

else
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % transformation of cpt into indexes (num_action,etat_var,etats_vois)
    % theta(cpt) <-> Dloc{num_action}(etat_var,etats_vois_list{:})

    n_actions=length(Dloc);

    % vector of the cumulated sizes of the blocks
    vect_tailles=zeros(1,n_actions);
    for i=1:length(Q)
        j=Q{i}(1);
       vect_tailles(i)=vect_tailles(i)+numel(Dloc{j});
       if ((i+1)<=n_actions)
        vect_tailles(i+1)=vect_tailles(i);
       end
    end
    num_partition=find(cpt<=vect_tailles,1);

    if(num_partition>1)
        cpt_new=cpt-vect_tailles(num_partition-1);
    else
        cpt_new=cpt;
    end
    
    j=Q{num_partition}(1);
    vect_indices=n_vec(cpt_new,size(Dloc{j}));

    etat_var=vect_indices(1);
    etats_vois=vect_indices(2:end);
    etats_vois_list=num2cell(etats_vois);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    Dloc_new=Dloc;

    %%%% change in Dloc due to theta(cpt)<-theta(cpt)+eps_df
    % modification of the coordinate corresponding to theta(cpt)
    for num_action=Q{num_partition}
        dk=Dloc{num_action}(etat_var,etats_vois_list{:});
        dk=(dk*exp(eps))/(1+(exp(eps)-1)*dk);
        Dloc_new{num_action}(etat_var,etats_vois_list{:})=dk;

        % modification of the other coordinates of the policy that need to be
        % changed
        long=length(Dloc{num_action}(:,etats_vois_list{:}));
        % scan all possible values for the action variable except etat_var
        vect=[1:etat_var-1,etat_var+1:long];
        for i=vect
            dg=Dloc{num_action}(i,etats_vois_list{:});
            dg=dg/(1+(exp(eps)-1)*dk);
            Dloc_new{num_action}(i,etats_vois_list{:})=dg;
        end
    end
    %%%%
end



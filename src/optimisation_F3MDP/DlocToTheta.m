function[theta]=DlocToTheta(Dloc,eps,Q)

% function for parametrization
% Dloc=exp(theta)/sum(exp(theta))
% theta_k=log(D_k)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% Dloc : factored stochastic policy
%% eps : parameter for the case of deterministic policies, in order to avoid
% log(0) ; typically, take eps=10^(-9)
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% representing the equality constraints on the parameters of the policy
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% theta : vecteur de parametres
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if Q is not given as input, assume it is empty
if(nargin<=2)
    Q=cell(1,0);
end


cpt_pol=1;
n_actions=length(Dloc);

if(isempty(Q))
    for i=1:n_actions
        cpt_pol_new=cpt_pol+numel(Dloc{i});
        theta(cpt_pol:cpt_pol_new-1)=log(Dloc{i}(:,:)+eps);    
        cpt_pol=cpt_pol_new;
    end
    
else
    for l=1:length(Q)
        i=Q{l}(1);
        cpt_pol_new=cpt_pol+numel(Dloc{i});
        theta(cpt_pol:cpt_pol_new-1)=log(Dloc{i}(:,:)+eps);    
        cpt_pol=cpt_pol_new;
    end
    
end

function[Dloc]=ThetaToDloc(f3mdp,theta,Q)

% reparametrization function for gradient descent
% Dloc=exp(theta)/sum(exp(theta))
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% theta : parameter vector representing the policy
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% representing the equality constraints on the parameters of the policy
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Dloc : stochastic factored policy associated with parameter theta
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% index for vector theta
cpt_pol=1;

Dloc=cell(1,f3mdp.n_actions);

if(isempty(Q))
    for j=1:f3mdp.n_actions 
        if(isempty(f3mdp.Ds{j}) && isempty(f3mdp.Da{j})) % precaution for particular case because prod([])=1        
            Dloc{j}=zeros(f3mdp.A(j),1);
            cpt_pol_new=cpt_pol+f3mdp.A(j)-1;
            mysum=sum(exp(theta(cpt_pol:cpt_pol_new)));
            Dloc{j}(:)=exp(theta(cpt_pol:cpt_pol_new))./mysum;
            cpt_pol=cpt_pol_new+1;

        else % classic case
            nb_etats_vois=prod(f3mdp.S(f3mdp.Ds{j}))*prod(f3mdp.A(f3mdp.Da{j}));
            Dloc{j}=zeros([f3mdp.A(j),f3mdp.S(f3mdp.Ds{j}),f3mdp.A(f3mdp.Da{j})]);

            for v=1:nb_etats_vois 
               % local states
               v_vec=n_vec(v,[f3mdp.S(f3mdp.Ds{j}),f3mdp.A(f3mdp.Da{j})]); 
               v_vec_list=num2cell(v_vec);

               cpt_pol_new=cpt_pol+f3mdp.A(j)-1;

               mysum=sum(exp(theta(cpt_pol:cpt_pol_new))); % may cause a numerical problem
               Dloc{j}(:,v_vec_list{:})=exp(theta(cpt_pol:cpt_pol_new))./mysum;

               cpt_pol=cpt_pol_new+1;

            end
        end  

    end
else
    
    for l=1:length(Q)        
        
     % cpt_pol_l : start index for partition l in vector theta 
     cpt_pol_l=cpt_pol;           

       for j=Q{l}                   
           
           if(isempty(f3mdp.Ds{j}) && isempty(f3mdp.Da{j})) % precaution for particular case because prod([])=1            
            Dloc{j}=zeros(f3mdp.A(j),1);
            cpt_pol_new=cpt_pol+f3mdp.A(j);
            mysum=sum(exp(theta(cpt_pol:cpt_pol_new-1)));
            Dloc{j}(:)=exp(theta(cpt_pol:cpt_pol_new-1))./mysum;
            
            else % classic case
                nb_etats_vois=prod(f3mdp.S(f3mdp.Ds{j}))*prod(f3mdp.A(f3mdp.Da{j}));
                Dloc{j}=zeros([f3mdp.A(j),f3mdp.S(f3mdp.Ds{j}),f3mdp.A(f3mdp.Da{j})]);

                for v=1:nb_etats_vois                                      
                    
                   % local states
                   v_vec=n_vec(v,[f3mdp.S(f3mdp.Ds{j}),f3mdp.A(f3mdp.Da{j})]); 
                   v_vec_list=num2cell(v_vec);

                   cpt_pol_new=cpt_pol+f3mdp.A(j);

                   mysum=sum(exp(theta(cpt_pol:cpt_pol_new-1))); % may cause a numerical problem
                   Dloc{j}(:,v_vec_list{:})=exp(theta(cpt_pol:cpt_pol_new-1))./mysum;
                   cpt_pol=cpt_pol_new; 
                   
                end % for v
                
           end  % end if
           
           cpt_pol=cpt_pol_l; 
           
       end % for j=Q{l}
       
       cpt_pol=cpt_pol_new;
       
    end % for l=1:length(Q)
    
    
end




function[policy,OUTPUT]=coordinate_descent_opt(f3mdp,Dloc0,INPUT)

% cyclic coordinate descent with use of fminbnd for 1D optimization
% on the problem with N' optimization variables
% need of binary action variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% Dloc0 : initial policy
%% INPUT structure
% gamma : discount factor
% h_f : 1 if finite horizon (with discount), else infinite horizon (with
% discount)
% eps : theoretical bound on the absolute error (case of the infinite horizon)
% it_max : horizon (case of the infinite or finite horizon)
% meth : method for libDAI
% param : parameters for libDAI
% add : 1 if additive rewards, else multiplicative
% pas : search step
% it_max_cd : maximum number of iterations for coordinate descent
% it_max_1D : maximum number of iterations for the 1D optimization
% mydisp : 1 if you want to display information
% mc : 1 if you want to compute the current values with Monte-Carlo approach in order to compare
% gmdp : 1 if the F^3MDP is a GMDP
% eval_with_mc : 1 if you want to use Monte-Carlo evaluation inside the
% algorithm instead of LBP evaluation
% n_simu_mc : number of simulations for Monte-Carlo evaluation
% mysave : 1 if you want to save the results 
% nom_fich : name of the file where saving the results
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUPUTS
%% policy : optimal policy
%% OUTPUT structure
% T : used horizon 
% V : vector of the values along the algoritm (for the problem with
% transformed reward), LBP or MC according to the value of eval_with_mc
% it : number of iterations (number of scans on all variables)
% Vmc : vector of the Monte-Carlo values along the algoritm (if mc=1)
% chgts : vecteur indicating the positions of the changes of coordinate
% time : time vector representing times where V changed (in order to plot V
% according to execution time)
% nb_eval : number of evaluations done during the algorithm
% Vt : LBP (or MC) value computed from the problem with transformed reward
% Vf : LBP (or MC) value computed from the problem with initial reward
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gamma=INPUT.gamma;
h_f=INPUT.h_f;
eps=INPUT.eps;
it_max=INPUT.it_max;
param=INPUT.param;
add=INPUT.add;
it_max_cd=INPUT.it_max_cd;
mydisp=INPUT.mydisp;
pdmg=INPUT.pdmg;
options=INPUT.options;
meth=INPUT.meth;

% Initialization
Dloc=Dloc0;

% verifications
if(add~=1)
   error('coordinate_descent : Attention!!! La methode 2 devaluation ne fonctionne que pour les recompenses additives.'); 
elseif(sum(f3mdp.A~=2)~=0)
   error('coordinate_descent : Attention!!! Les variables daction doivent etre binaires.'); 
end


% transformation of reward
[f3mdp.Rloc,r_max,r_max_new]=transform_reward(f3mdp.Rloc);

% stopping time for the F^3MDP with transformed reward
if(h_f==1)
    T=it_max;
else 
    % infinite horizon : must work with absolute error in order to be able
    % to know the stopping time in advance
    T=arret(gamma,eps,r_max_new);
end

% Construction of the factor graph
fg=initialize_factor_graph_add(f3mdp,Dloc0,pdmg);
for t=1:T 
    fg=allonge_factor_graph_add(fg,f3mdp,Dloc0,pdmg);
end

% preallocation of the vectors
% we don't know their size in advance but it is better to preallocate
V=zeros(1,it_max_cd*100);
time=zeros(1,it_max_cd*100);
chgts=zeros(1,it_max_cd*100);

%%%% Optimization by coordinate descent %%%%
[V(1),T]=evaluation(f3mdp,Dloc0,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,pdmg); % temps 0
% start the chronometer
tic

if(mydisp)
    V(1)
end

% while one of the coordinates of the policy progresses
changement=true;
it=1;
k=1; % index for V 
c=0; % index for chgts
nb_eval=1;
while(changement && it<it_max_cd)
    changement=false;
    if(mydisp)
        it
    end
    
    for j=1:f3mdp.n_actions
       nb_etats_vois=prod(f3mdp.S(f3mdp.Ds{j}))*prod(f3mdp.A(f3mdp.Da{j}));
       
       % scan all possible states for the neighbours
       for v=1:nb_etats_vois
           % local states
           v_vec=n_vec(v,[f3mdp.S(f3mdp.Ds{j}),f3mdp.A(f3mdp.Da{j})]); 
           v_vec_list=num2cell(v_vec);           
           
           % 1D objective function
           obj1D=@(policy_param)eval_1D(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,add,r_max,fg,pdmg,policy_param,j,v_vec_list);
           
           % We use fminbnd           
           [x,Vnew,EXITFLAG,OUTPUT.fminbnd]=fminbnd(obj1D,0,1,options);       
           nb_eval=nb_eval+OUTPUT.fminbnd.funcCount;
           
           % We record the change (if there's a real improvement)
           if(Vnew<V(k))
               k=k+1;
               V(k)=Vnew;
               Dloc{j}(1,v_vec_list{:})=x;
               Dloc{j}(2,v_vec_list{:})=1-x;
               time(k)=toc;
               changement=true;
                if(mydisp)
                    V(k)
                end
           end
           
           % We record the change of coordinate
           c=c+1;
           chgts(c)=k+1;
       end
    end
    it=it+1;    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Returned policy
policy=Dloc;

% Return to the initial reward
B=r_max/(1-gamma);
OUTPUT.Vf=B-V(k);

% OUTPUTS
OUTPUT.T=T;
OUTPUT.it=it-1;
OUTPUT.chgts=chgts(1:c);
OUTPUT.time=time(1:k);
OUTPUT.nb_eval=nb_eval;
OUTPUT.V=V(1:k);




function[policy,OUTPUT]=coordinate_descent(f3mdp,Dloc0,INPUT,Q)

% cyclic coordinate descent
% on the problem with N' optimization variables
% need of binary action variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% f3mdp : the F^3MDP
%% Dloc0 : initial policy
%% INPUT structure
% gamma : discount factor
% h_f : 1 if finite horizon (with discount), else infinite horizon (with
% discount)
% eps : theoretical bound on the absolute error (case of the infinite horizon)
% it_max : horizon (case of the infinite or finite horizon)
% meth : method for libDAI
% param : parameters for libDAI
% add : 1 if additive rewards, else multiplicative
% pas : search step
% it_max_cd : maximum number of iterations for coordinate descent
% it_max_1D : maximum number of iterations for the 1D optimization
% mydisp : 1 if you want to display information
% mc : 1 if you want to compute the current values with Monte-Carlo approach in order to compare
% gmdp : 1 if the F^3MDP is a GMDP
% eval_with_mc : 1 if you want to use Monte-Carlo evaluation inside the
% algorithm instead of LBP evaluation
% n_simu_mc : number of simulations for Monte-Carlo evaluation
% mysave : 1 if you want to save the results 
% nom_fich : name of the file where saving the results
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% representing the equality constraints on the parameters of the policy
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUPUTS
%% policy : optimal policy
%% OUTPUT structure
% T : used horizon 
% V : vector of the values along the algoritm (for the problem with
% transformed reward), LBP or MC according to the value of eval_with_mc
% it : number of iterations (number of scans on all variables)
% Vmc : vector of the Monte-Carlo values along the algoritm (if mc=1)
% chgts : vecteur indicating the positions of the changes of coordinate
% time : time vector representing times where V changed (in order to plot V
% according to execution time)
% nb_eval : number of evaluations done during the algorithm
% Vt : LBP (or MC) value computed from the problem with transformed reward
% Vf : LBP (or MC) value computed from the problem with initial reward
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% if Q is not given, it is supposed to be empty
if(nargin<=3)
    Q=cell(1,0);
end

% if Q is not empty, use coordinate_descent_ql function
if(~isempty(Q))
    [policy,OUTPUT]=coordinate_descent_q(f3mdp,Dloc0,INPUT,Q);

else

% inputs
gamma=INPUT.gamma;
h_f=INPUT.h_f;
eps=INPUT.eps;
it_max=INPUT.it_max;
param=INPUT.param;
add=INPUT.add;
pas=INPUT.pas;
it_max_cd=INPUT.it_max_cd;
it_max_1D=INPUT.it_max_1D;
mydisp=INPUT.mydisp;
mc=INPUT.mc;
gmdp=INPUT.gmdp;
eval_with_mc=INPUT.eval_with_mc;
n_simu_mc=INPUT.n_simu_mc;
meth=INPUT.meth;
mysave=INPUT.save;
nom_fich=INPUT.nom_fich;

% verifications
if(add~=1)
   error('coordinate_descent : Caution!!! The evaluation method works only for additive rewards.'); 
elseif(sum(f3mdp.A~=2)~=0)
   error('coordinate_descent : Caution!!! The action variables must be binary.'); 
end

% if we evaluate with MC, no need of MC for comparison
if(eval_with_mc)
    mc=0;
end

% Policy initialization
policy=Dloc0;

% transformation of reward
Rloc_init=f3mdp.Rloc;
[f3mdp.Rloc,r_max,r_max_new]=transform_reward(f3mdp.Rloc);

% stopping time for the F^3MDP with transformed reward
if(h_f==1)
    T=it_max;
else
    % infinite horizon : must work with absolute error in order to be able
    % to know the stopping time in advance
    T=arret(gamma,eps,r_max_new);
end

% Construction of the factor graph
if(~eval_with_mc)
    fg=create_factor_graph(f3mdp,Dloc0,gmdp,T);
end

% preallocation of the vectors
% we don't know their size in advance but it is better to preallocate
V=zeros(1,it_max_cd*100);
time=zeros(1,it_max_cd*100);
chgts=zeros(1,it_max_cd*100);
if(mc)
    Vmc=zeros(1,it_max_cd*100);
else
    Vmc=[];
end

%%%% Optimization by coordinate descent %%%%
% start the chronometer
tic

if(eval_with_mc)
    V(1)=eval_mc_par(f3mdp,Dloc0,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea); 
else
    [V(1),T]=evaluation(f3mdp,Dloc0,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,gmdp); 
end

if(mc)
  Vmc(1)=eval_mc_par(f3mdp,Dloc0,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add); 
end
if(mydisp)
    V(1)
end

% while one of the coordinates of the policy progresses
changement=true;
it=1;
k=1; % index for V and Vmc
c=0; % index for chgts
nb_eval=1;
while(changement && it<it_max_cd)
    changement=false;
    if(mydisp)
        it
    end
    
    for j=1:f3mdp.n_actions
       nb_etats_vois=prod(f3mdp.S(f3mdp.Ds{j}))*prod(f3mdp.A(f3mdp.Da{j}));
       
       % scan all possible states for the neighbours
       for v=1:nb_etats_vois
           % local states
           v_vec=n_vec(v,[f3mdp.S(f3mdp.Ds{j}),f3mdp.A(f3mdp.Da{j})]); 
           v_vec_list=num2cell(v_vec);
           
           %%%% look for the right direction
           % if possible to turn right
           if(policy{j}(1,v_vec_list{:})+pas>0  && policy{j}(1,v_vec_list{:})+pas<=1)
               policy_new=policy;
               policy_new{j}(1,v_vec_list{:})=policy_new{j}(1,v_vec_list{:})+pas;
               policy_new{j}(2,v_vec_list{:})=1-policy_new{j}(1,v_vec_list{:});
               Dloc=policy_new;
               if(eval_with_mc)
                   Vnew=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea);
               else
                   Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp);
               end
               nb_eval=nb_eval+1;
           
               if(Vnew<V(k))
                   k=k+1;
                   V(k)=Vnew;
                   time(k)=toc;
                   
                   policy=policy_new;
                   sens=1;
                   changement=true;
                   if(mc)
                      Vmc(k)=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea); 
                   end
                   if(mydisp)
                     V(k)
                   end
               elseif(policy{j}(1,v_vec_list{:})-pas>0  && policy{j}(1,v_vec_list{:})-pas<=1)
                   policy_new=policy;
                   policy_new{j}(1,v_vec_list{:})=policy_new{j}(1,v_vec_list{:})-pas;
                   policy_new{j}(2,v_vec_list{:})=1-policy_new{j}(1,v_vec_list{:});
                   Dloc=policy_new;
                   if(eval_with_mc)
                     Vnew=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea);
                   else
                     Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp);
                   end
                   nb_eval=nb_eval+1;
                   
                   if(Vnew<V(k))
                       k=k+1;
                       V(k)=Vnew;
                       time(k)=toc;
                                             
                       policy=policy_new;
                       sens=-1;
                       changement=true;
                       if(mc)
                        Vmc(k)=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea); 
                        end
                       if(mydisp)
                            V(k)
                        end
                   else
                       % already optimal on this coordinate
                       continue; % come back line 50 
                   end 
	       else        
			continue;  % come back line 50 
               end
           % if it is not possible to turn right, we try to turn left
           elseif(policy{j}(1,v_vec_list{:})-pas>0  && policy{j}(1,v_vec_list{:})-pas<=1)               
               policy_new=policy;
               policy_new{j}(1,v_vec_list{:})=policy_new{j}(1,v_vec_list{:})-pas;
               policy_new{j}(2,v_vec_list{:})=1-policy_new{j}(1,v_vec_list{:});
               Dloc=policy_new;
               if(eval_with_mc)
                   Vnew=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea);
               else
                   Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp);
               end
               nb_eval=nb_eval+1;
               %%% if it works
               if(Vnew<V(k))
                   k=k+1;
                   V(k)=Vnew;
                   time(k)=toc;

                   policy=policy_new;
                   sens=-1;
                   changement=true;
                   if(mc)
                    Vmc(k)=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea); 
                    end
                   if(mydisp)
                        V(k)
                   end
               %%% if it doesn't work
               else
                   % we change the coordinate
                   continue; % come back line 50 
               end
	   else
                   continue; % come back line 50 
           end
           
           % we continue in this direction
           amelioration=true;
           % count the number of inner iterations (for 1D optimization)
           it_int=1;
           while(amelioration && it_int<it_max_1D)
               amelioration=false;
               if(policy{j}(1,v_vec_list{:})+sens*pas>0  && policy{j}(1,v_vec_list{:})+sens*pas<=1)
                   policy_new=policy;
                   policy_new{j}(1,v_vec_list{:})=policy_new{j}(1,v_vec_list{:})+sens*pas;
                   policy_new{j}(2,v_vec_list{:})=1-policy_new{j}(1,v_vec_list{:});
                   Dloc=policy_new;
                   if(eval_with_mc)
                    Vnew=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea);
                   else
                    Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp);
                   end
                   nb_eval=nb_eval+1;
                   if(Vnew<V(k))
                       k=k+1;
                       V(k)=Vnew;
                       time(k)=toc;
                                              
                       policy=policy_new;
                       amelioration=true;
                       changement=true;
                       if(mc)
                         Vmc(k)=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea); 
                        end
                       if(mydisp)
                           V(k)
                       end
                   end
               end
               it_int=it_int+1;
           end
           % memorize the change of coordinate
           c=c+1;
           chgts(c)=k+1;
           
       end
    end
    it=it+1;
    if(mysave)
        % Return to the initial problem
        B=r_max/(1-gamma);
        OUTPUT.Vt=B-V(k);

        % OUTPUTS
        OUTPUT.T=T;
        OUTPUT.it=it-1;
        OUTPUT.chgts=chgts(1:c);
        OUTPUT.time=time(1:k);
        OUTPUT.nb_eval=nb_eval;
        OUTPUT.V=V(1:k);
        if(mc)
            OUTPUT.Vmc=Vmc(1:k);
        end
        save(nom_fich,'policy','OUTPUT');
    end
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Return to the initial problem
B=r_max/(1-gamma);
OUTPUT.Vt=B-V(k); 

% LBP value for initial reward
f3mdp.Rloc=Rloc_init;
if(eval_with_mc)
    OUTPUT.Vf=eval_mc_par(f3mdp,Dloc,n_simu_mc,gamma,h_f,eps,it_max,r_max,1,add,f3mdp.ordres,f3mdp.ordrea);
else
    [OUTPUT.Vf,~]=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp); 
end
% OUTPUTS
OUTPUT.T=T;
OUTPUT.it=it-1;
OUTPUT.chgts=chgts(1:c);
OUTPUT.time=time(1:k);
OUTPUT.nb_eval=nb_eval;
OUTPUT.V=V(1:k);
if(mc)
    OUTPUT.Vmc=Vmc(1:k);
end
end
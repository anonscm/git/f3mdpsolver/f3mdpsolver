function[Q,Ds]=symmetry_grid(n)

% gives the partition Q representing equality constraints for the parameters
% of the policy and associated to symetries of a grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% n : size of the grid
% there are n^2 cells
% we suppose there is one action variable per cell of the grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% representing the equality constraints on the parameters of the policy
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%% Ds : if you want a policy based on the states of the neighbourhood, use this output ; the cell itself is first, and then the ones at distance 1
% (different from using the grid_neigh function with default options)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% EXAMPLE : grid 5x5 (n=5)
% L=sum(1:round(n/2))=6
% Q{1}={1,5,21,25}
% Q{2}={2,4,6,10,16,20,22,24}
% Q{3}={3,11,15,23}
% Q{4}={7,9,17,19}
% Q{5}={8,12,14,18}
% Q{6}={13}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ds=grid_neigh(n,0);

% length of the partition
L=sum(1:round(n/2));

% partition
Q=cell(1,L);

% center
i=(n+1)/2; 

l=1;

if(mod(n,2)==0) % n even
    for k=0.5:(n/2)
       for j=k:(n/2)
           abscisses=[i+j,i+j,i-j,i-j,i+k,i+k,i-k,i-k];
           ordonnees=[i+k,i-k,i+k,i-k,i+j,i-j,i+j,i-j];
           % sub2ind considers that cells are indexed by column whereas I
           % consider they are indexed by line, that's why I need to invert
           % abscissa and ordinate when I use the function
           Q{l}=unique(sub2ind([n,n],ordonnees,abscisses));
           l=l+1;
       end

    end
    
else % n odd
    for k=0:((n-1)/2)
       for j=k:((n-1)/2)
           abscisses=[i+j,i+j,i-j,i-j,i+k,i+k,i-k,i-k];
           ordonnees=[i+k,i-k,i+k,i-k,i+j,i-j,i+j,i-j];
           % sub2ind considers that cells are indexed by column whereas I
           % consider they are indexed by line, that's why I need to invert
           % abscissa and ordinate when I use the function
           Q{l}=unique(sub2ind([n,n],ordonnees,abscisses));
           l=l+1;
       end

    end
end
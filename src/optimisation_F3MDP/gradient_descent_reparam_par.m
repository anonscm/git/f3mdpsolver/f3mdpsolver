function[policy,OUTPUT]=gradient_descent_reparam_par(f3mdp,theta0,INPUT,Q)

% gradient descent : parallel version
% on the reparametrized problem with N optimization variables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% theta0 : initial parameter associated with Dloc0
%% INPUT structure
% gamma : discount factor
% h_f : 1 if finite horizon (with discount), else infinite horizon (with discount)
% eps : theoretical bound on the absolute error (case of the infinite horizon)
% it_max : horizon (case of the finite or infinite horizon)
% meth : method for libDAI
% param : parameters for libDAI
% add : 1 additive rewards, else multiplicative
% it_max_gd : number of maximum iterations for gradient descent
% eps_gd_V : precision parameter for the stopping criterion on the value
% eps_gd_theta : precision parameter for the stopping criterion on the parameter
% eps_gd_grad : precision parameter for the stopping criterion on the norm of the gradient
% mydisp : 1 for displaying information
% gmdp : 1 si the f3mdp is a gmdp
% eps_df : precision parameter for finite differences
% type_step : 'fixed', 'wolfe' or 'optimal'
% eps1 : parameter for the armijo condition (typically 10^(-4))
% eps2 : parameter for the wolfe condition (typically 0.99)
% it_max_rl : number of maximum iterations for line search (for line search based on Wolfe's method)
% step : step for line research (case of fixed step)
% options : options for fminbnd (case of optimal step)
% borne_max_pas_opt : maximum bound for fminbnd (case of optimal step)
% mysave : 1 to save the results along the algorithm
% nom_fich : name of the file for saving results
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% representing the equality constraints on the parameters of the policy
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS 
%% policy : optimal policy
%% OUTPUT structure
% fminbnd : last output of fminbnd function
% Vt : LBP value computed from the problem with transformed reward
% Vf : LBP value computed from the problem with transformed reward
% T : horizon used
% V : vector of the values along the algoritm (for the problem with
% transformed reward), vector of length it+1
% it : number of done iterations (=0 if the norm of the gradient is null for the initial parameter)
% pas : vector of the done steps (vector of length it+1 awhose first value is null)
% Dloc0 : initial policy
% theta : last value of parameter theta
% grad : last value for the gradient of theta
% norme_grad : norm of the gradient along the algorithm, vector of length it+1
% diff_V : vector of abs(Vnew-V))/(1+abs(V))) along the algorithm, vector of length it+1 whose first value is null
% diff_theta : vector of norm(theta_new-theta)/(1+norm(theta)) along the algorithm, vector of length it+1 whose first value is null
% time_grad : last computation time for the gradient
% time_iter : last time for one iteration of the algorithm
% termination : information about termination of the algorithm
% n_eval : number of evaluations done during the algorithm
% time : vector of times where an update has been done (allows to draw value according to execution time), vector of length it+1
% nb_param : number of parameters (length of vector theta)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% inputs
gamma=INPUT.gamma;
h_f=INPUT.h_f;
eps=INPUT.eps;
it_max=INPUT.it_max;
param=INPUT.param;
add=INPUT.add;
it_max_gd=INPUT.it_max_gd;
eps_gd_V=INPUT.eps_gd_V;
eps_gd_theta=INPUT.eps_gd_theta;
eps_gd_grad=INPUT.eps_gd_grad;
mydisp=INPUT.mydisp;
gmdp=INPUT.gmdp;
eps_df=INPUT.eps_df;
eps1=INPUT.eps1;
eps2=INPUT.eps2;
it_max_rl=INPUT.it_max_rl;
type_step=INPUT.type_step;
step=INPUT.step;
options=INPUT.options;
borne_max_pas_opt=INPUT.borne_max_pas_opt;
meth=INPUT.meth;
mysave=INPUT.save;
nom_fich=INPUT.nom_fich;

% verifications
if(add~=1)
   error('gradient_descent_reparam : Attention!!! La methode 2 devaluation ne fonctionne que pour les recompenses additives.'); 
end

% if Q is not present in inputs, consider it is empty
if(nargin<=3)
    Q=cell(1,0);
end

% number of parameters
[nb_param,~]=compute_nb_param(f3mdp,Q);

% initialization
theta=theta0;
grad=theta0;
[Dloc0]=ThetaToDloc(f3mdp,theta,Q);
policy=Dloc0;
Dloc=Dloc0;

% transformation of reward
Rloc_init=f3mdp.Rloc;
[f3mdp.Rloc,r_max,r_max_new]=transform_reward(f3mdp.Rloc);
disp('Reward transformed')

% stopping time for the f3mdp with transformed reward
if(h_f==1)
    T=it_max;
else 
    % infinite horizon : must work with absolute error in order to be able
    % to know the stopping time in advance
    T=arret(gamma,eps,r_max_new);
end

% Construction of the factor graph 
fg=create_factor_graph(f3mdp,Dloc0,gmdp,T);
disp('Factor graph constructed')

V=zeros(1,it_max_gd);
pas=zeros(1,it_max_gd);
norme_grad=zeros(1,it_max_gd);
time=zeros(1,it_max_gd);
diff_V=zeros(1,it_max_gd);
diff_theta=zeros(1,it_max_gd);

depart=tic;
[V(1),T]=evaluation(f3mdp,Dloc0,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,gmdp);
n_eval=1;
k=1;

disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
disp('Iteration :')
disp(k-1)

if(mydisp)   
   theta
   disp('V=')
   disp(V(k))
end

%%%%%% Gradient descent
non_convergence=1;
while(non_convergence) 

    %%%%%%%%%%%%%%%%%%%%%% computation of the gradient %%%%%%%%%%%%%%%%%%%%%%
    debut_grad=tic;
    debut_iter=tic;
    grad_old=grad;
    grad=zeros(1,length(theta)); 

    Vold=V(k);

    % cpt : loop variable
    % grad : output sliced variable
    % f3mdp,policy,eps_df,Vold... : broadcast variables
    % Vnew,Dloc2 : temporary variables  
    parfor cpt=1:nb_param
       Dloc2=local_modif(policy,cpt,eps_df,Q);
       Vnew=evaluation(f3mdp,Dloc2,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,gmdp);           
       grad(cpt)=(Vnew-Vold)/eps_df;
    end  
    n_eval=n_eval+nb_param;

    time_grad=toc(debut_grad);
           
    
    % update the norm of the gradient
    norme_grad(k)=norm(grad);
    
    if(mydisp)        
        grad
       disp('Norm of the gradient :') 
       disp(norm(grad))
    end
    
    % if null gradient  -> finished!
    if(norm(grad)<eps_gd_grad) 
        disp('Gradient descent stopped by local optimality condition (norm of the gradient near to 0).');
        non_convergence=0;
        termination='null gradient';
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    


    %%%%%%%%%%%%%%%%%%%%%%%% line search %%%%%%%%%%%%%%%%%%%%%%%%
    if(non_convergence)
        
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        disp('Iteration :')
        disp(k)
        
        
        if(strcmp(type_step,'fixed')) % fixed step
            theta_new=theta-step*grad/norm(grad);
            pas(k+1)=step;
            Dloc=ThetaToDloc(f3mdp,theta_new,Q);
            Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,gmdp);  
            n_eval=n_eval+1;

        elseif(strcmp(type_step,'optimal')) % optimal step
            % it seems we cannot give an initial value to fminbnd
            borne_min=0;
            obj1D=@(pas)eval_reparam_step(f3mdp,gamma,h_f,eps,it_max,meth,param,add,r_max,fg,gmdp,theta,grad,pas,Q);
            [sopt,Vnew,EXITFLAG,OUTPUT.fminbnd]=fminbnd(obj1D,borne_min,borne_max_pas_opt,options);
            n_eval=n_eval+OUTPUT.fminbnd.funcCount;
            theta_new=theta-sopt*grad/norm(grad);
            pas(k+1)=sopt;
            Dloc=ThetaToDloc(f3mdp,theta_new,Q);
            % Vnew was already computed with fminbnd

        else % Wolfe's line search

            % initialization
            s=zeros(1,it_max_rl);
            if(k>=2)
                s(1)=pas(k); % initialize with previous step
            else % the first time we look for the optimal step
                borne_min=0;
                obj1D=@(pas)eval_reparam_step(f3mdp,gamma,h_f,eps,it_max,meth,param,add,r_max,fg,gmdp,theta,grad,pas,Q);
                [sopt,Vnew,EXITFLAG,OUTPUT.fminbnd]=fminbnd(obj1D,borne_min,borne_max_pas_opt,options);
                n_eval=n_eval+OUTPUT.fminbnd.funcCount;
                s(1)=sopt;
            end
            pas(k+1)=s(1);
            if(mydisp)
                disp('pas :')
                pas(k+1)
            end

            k_rl=1;
            borne_min=0;
            borne_max=realmax;
            theta_new=theta-s(1)*grad/norm(grad);

            Dloc=ThetaToDloc(f3mdp,theta_new,Q);
            Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,gmdp); 
            n_eval=n_eval+1;

            % verification of the conditions
            armijo_nok=Vnew>(V(k)+eps1*s(1)*norm(grad)^2);        
            wolfe_nok=dot(grad,grad_old)>eps2*norm(grad_old)^2;

            while( (armijo_nok||wolfe_nok) && k_rl<it_max_rl) 
                % Armijo : in order to avoid too long steps
                if(armijo_nok)
                    borne_max=s(k_rl);
                    k_rl=k_rl+1;
                    s(k_rl)=(borne_min+borne_max)/2;
                    theta_new=theta-s(k_rl)*grad/norm(grad);        
                    Dloc=ThetaToDloc(f3mdp,theta_new,Q);
                    Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,gmdp); 
                    n_eval=n_eval+1;
                    pas(k+1)=s(k_rl);
                    disp('Modification of the step by Armijo condition');
                    if(mydisp)
                       k_rl
                       s(k_rl)
                    end

                % Wolfe : in order to avoid too small steps
                % we do the control a posteriori because otherwise we would
                % have to compute a gradient (N evaluations)
                % if the step of the previous iteration was too small, as
                % now it is not too long and it is the sames step, we
                % modify it (even if we are not sure that it is too small for this present iteration)
                elseif(wolfe_nok && k>1) % if it is the first iteration, we cannot check Wolfe condition
                   borne_min=s(k_rl); 
                   k_rl=k_rl+1;
                   if(borne_max<realmax)
                      s(k_rl)=(borne_min+borne_max)/2;
                   else
                      s(k_rl)=2*borne_min;
                   end
                   disp(['The step of previous iteration (k='  num2str(k-1) ') was too small according to Wolfe condition.']); 
                   pas(k+1)=s(k_rl);
                   theta_new=theta-s(k_rl)*grad/norm(grad);        
                    Dloc=ThetaToDloc(f3mdp,theta_new,Q);
                    Vnew=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max_new,add,fg,gmdp);
                    n_eval=n_eval+1;
                    disp('Modification of the step.');
                   if(mydisp)
                       k_rl
                       s(k_rl)
                    end
                end     

                armijo_nok=Vnew>V(k)+eps1*s(k_rl)*norm(grad)^2;
                % it is not worth looking at Wolfe's condition again
                % wolfe_nok=dot(grad,grad_old)>eps2*norm(grad_old)^2;
                wolfe_nok=0;
            end     

        end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % convergence
        diff_V(k+1)=abs(Vnew-V(k))/(1+abs(V(k)));
        diff_theta(k+1)=norm(theta_new-theta)/(1+norm(theta));
        
        if(k>it_max_gd)
            disp('Gradient descent stopped because maximum number of iterations has been reached.');
            termination='maxit';
            non_convergence=0;
        elseif(norme_grad(k)<eps_gd_grad)
            disp('Gradient descent stopped by local optimality condition (norm of the gradient near to 0).');
            non_convergence=0;
            termination='null gradient';
        elseif(diff_V(k+1)<eps_gd_V && diff_theta(k+1)<eps_gd_theta)
            disp('Gradient descent stopped because of stagnation of objective function and parameter.');
            termination='stagnation objective and theta';
            non_convergence=0;
        end



        % update
        theta=theta_new;
        policy=Dloc;    
        k=k+1;
        V(k)=Vnew;  
        time(k)=toc(depart);

       if(mydisp)
            theta
            disp('V=')
            disp(V(k))
        end
    end
    
    time_iter=toc(debut_iter);
    
    % save intermediate results
    if(mysave)
        % Return to the initial problem
        B=r_max/(1-gamma);
        OUTPUT.Vt=B-V(k);

        % OUTPUTS
        OUTPUT.T=T;
        OUTPUT.V=V(1:k);
        OUTPUT.it=k-1;
        OUTPUT.pas=pas(1:k);
        OUTPUT.Dloc0=Dloc0;
        OUTPUT.theta=theta;
        OUTPUT.grad=grad;
        OUTPUT.norme_grad=norme_grad(1:k);
        OUTPUT.diff_V=diff_V(1:k);
        OUTPUT.diff_theta=diff_theta(1:k);
        OUTPUT.time_grad=time_grad;
        OUTPUT.time_iter=time_iter;
        OUTPUT.n_eval=n_eval;
        OUTPUT.time=time(1:k);
        OUTPUT.nb_param=nb_param;
        
        save(nom_fich,'policy','OUTPUT');
    end                       
end

% Return to the initial problem
B=r_max/(1-gamma);
OUTPUT.Vt=B-V(k);

% LBP value for initial reward
f3mdp.Rloc=Rloc_init;
[OUTPUT.Vf,~]=evaluation(f3mdp,Dloc,gamma,h_f,eps,it_max,meth,param,r_max,add,fg,gmdp);

% OUTPUTS
OUTPUT.T=T;
OUTPUT.V=V(1:k);
OUTPUT.it=k-1;
OUTPUT.pas=pas(1:k); 
OUTPUT.Dloc0=Dloc0;
OUTPUT.theta=theta;
OUTPUT.grad=grad;
OUTPUT.norme_grad=norme_grad(1:k);
OUTPUT.diff_V=diff_V(1:k);
OUTPUT.diff_theta=diff_theta(1:k);
OUTPUT.time_grad=time_grad;
OUTPUT.time_iter=time_iter;
OUTPUT.termination=termination;
OUTPUT.n_eval=n_eval;
OUTPUT.time=time(1:k);
OUTPUT.nb_param=nb_param;

if(mysave)
    save(nom_fich,'policy','OUTPUT');
end

% display figures
if(mydisp)

    figure(1);
    plot(0:k-1,V(1:k));
    xlabel('iteration');
    ax=gca;
    ax.XTick = 0:k-1;
    ylabel('V');
    title('Gradient descent');

    figure(2);
    plot(0:k-1,norme_grad(1:k)); 
    xlabel('iteration');
    ax=gca;
    ax.XTick = 0:k-1;
    ylabel('norm of the gradient');
    title('Norm of the gradient');

end 

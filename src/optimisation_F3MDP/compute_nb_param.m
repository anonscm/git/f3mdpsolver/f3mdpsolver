function[N,Nprime]=compute_nb_param(f3mdp,Q)

% computes the number of parameters of the optimization problem (complete :
% N or reduced : Nprime)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% f3mdp : the F^3MDP
%% Q : partition of {1,...,m}, the set of the indexes of the action variables
% Q is a list of length L
% Q{i}=vector of the indexes in Q_i
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% N : number of parameters in the optimization problem
%% Nprime : number of parameters in the equivalent optimization problem with
% less parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N=0;
Nprime=0;

if(isempty(Q))
    for j=1:f3mdp.n_actions
        x=prod(f3mdp.S(f3mdp.Ds{j}))*prod(f3mdp.A(f3mdp.Da{j}));
        N=N+f3mdp.A(j)*x;
        Nprime=Nprime+(f3mdp.A(j)-1)*x;
    end
else
    for i=1:length(Q)
        j=Q{i}(1);
        x=prod(f3mdp.S(f3mdp.Ds{j}))*prod(f3mdp.A(f3mdp.Da{j}));
        N=N+f3mdp.A(j)*x;
        Nprime=Nprime+(f3mdp.A(j)-1)*x;
    end
end
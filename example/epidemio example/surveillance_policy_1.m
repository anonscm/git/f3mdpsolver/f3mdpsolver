function[Dloc]=surveillance_policy_1()

% policy for the 5x5 epidemiologic problem with surveillance network
% consists in treating if the nearest field(s) in the surveillance network
% are infected
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Dloc : stochastic factored policy

Dloc=cell(1,25);

%%% field 1
Dloc{1}=zeros(2,2,2,2,2);
% if field 7 is infected treat
Dloc{1}(2,2,:,:,:)=1;
Dloc{1}(1,1,:,:,:)=1;

%%% field 2 : idem
Dloc{2}=zeros(2,2,2,2,2);
Dloc{2}(2,2,:,:,:)=1;
Dloc{2}(1,1,:,:,:)=1;

%%% field 3 
Dloc{3}=zeros(2,2,2,2,2);
% if field 7 or field 9 is infected treat
Dloc{3}(2,2,:,:,:)=1;
Dloc{3}(2,:,2,:,:)=1;
Dloc{3}(1,1,1,:,:)=1;

%%% field 4 : base on field 9
Dloc{4}=zeros(2,2,2,2,2);
Dloc{4}(2,:,2,:,:)=1;
Dloc{4}(1,:,1,:,:)=1;

%%% field 5 : idem
Dloc{5}=zeros(2,2,2,2,2);
Dloc{5}(2,:,2,:,:)=1;
Dloc{5}(1,:,1,:,:)=1;

%%% field 6 : base on field 7
Dloc{6}=zeros(2,2,2,2,2);
Dloc{6}(2,2,:,:,:)=1;
Dloc{6}(1,1,:,:,:)=1;

%%% field 7 : idem
Dloc{7}=zeros(2,2,2,2,2);
Dloc{7}(2,2,:,:,:)=1;
Dloc{7}(1,1,:,:,:)=1;

%%% field 8 : base on fields 7 and 9
Dloc{8}=zeros(2,2,2,2,2);
Dloc{8}(2,2,:,:,:)=1;
Dloc{8}(2,:,2,:,:)=1;
Dloc{8}(1,1,1,:,:)=1;

%%% field 9 : base on field 9
Dloc{9}=zeros(2,2,2,2,2);
Dloc{9}(2,:,2,:,:)=1;
Dloc{9}(1,:,1,:,:)=1;

%%% field 10 : base on field 9
Dloc{10}=zeros(2,2,2,2,2);
Dloc{10}(2,:,2,:,:)=1;
Dloc{10}(1,:,1,:,:)=1;

%%% field 11 : base on fields 7 and 17
Dloc{11}=zeros(2,2,2,2,2);
Dloc{11}(2,2,:,:,:)=1;
Dloc{11}(2,:,:,2,:)=1;
Dloc{11}(1,1,:,1,:)=1;

%%% field 12 : base on fields 7 and 17
Dloc{12}=zeros(2,2,2,2,2);
Dloc{12}(2,2,:,:,:)=1;
Dloc{12}(2,:,:,2,:)=1;
Dloc{12}(1,1,:,1,:)=1;

%%% field 13 : base on the 4 fields
Dloc{13}=zeros(2,2,2,2,2);
Dloc{13}(2,2,:,:,:)=1;
Dloc{13}(2,:,2,:,:)=1;
Dloc{13}(2,:,:,2,:)=1;
Dloc{13}(2,:,:,:,2)=1;
Dloc{13}(1,1,1,1,1)=1;

%%% field 14 : base on fields 9 and 19
Dloc{14}=zeros(2,2,2,2,2);
Dloc{14}(2,:,2,:,:)=1;
Dloc{14}(2,:,:,:,2)=1;
Dloc{14}(1,:,1,:,1)=1;

%%% field 15 : base on fields 9 and 19
Dloc{15}=zeros(2,2,2,2,2);
Dloc{15}(2,:,2,:,:)=1;
Dloc{15}(2,:,:,:,2)=1;
Dloc{15}(1,:,1,:,1)=1;

%%% field 16 : base on field 17
Dloc{16}=zeros(2,2,2,2,2);
Dloc{16}(2,:,:,2,:)=1;
Dloc{16}(1,:,:,1,:)=1;

%%% field 17 : base on field 17
Dloc{17}=zeros(2,2,2,2,2);
Dloc{17}(2,:,:,2,:)=1;
Dloc{17}(1,:,:,1,:)=1;

%%% field 18 : base on fields 17 and 19
Dloc{18}=zeros(2,2,2,2,2);
Dloc{18}(2,:,:,2,:)=1;
Dloc{18}(2,:,:,:,2)=1;
Dloc{18}(1,:,:,1,1)=1;

%%% field 19 : base on field 19
Dloc{19}=zeros(2,2,2,2,2);
Dloc{19}(2,:,:,:,2)=1;
Dloc{19}(1,:,:,:,1)=1;

%%% field 20 : base on field 19
Dloc{20}=zeros(2,2,2,2,2);
Dloc{20}(2,:,:,:,2)=1;
Dloc{20}(1,:,:,:,1)=1;

%%% field 21 : base on field 17
Dloc{21}=zeros(2,2,2,2,2);
Dloc{21}(2,:,:,2,:)=1;
Dloc{21}(1,:,:,1,:)=1;

%%% field 22 : base on field 17
Dloc{22}=zeros(2,2,2,2,2);
Dloc{22}(2,:,:,2,:)=1;
Dloc{22}(1,:,:,1,:)=1;

%%% field 23 : base on fields 17 and 19
Dloc{23}=zeros(2,2,2,2,2);
Dloc{23}(2,:,:,2,:)=1;
Dloc{23}(2,:,:,:,2)=1;
Dloc{23}(1,:,:,1,1)=1;

%%% field 24 : base on field 19
Dloc{24}=zeros(2,2,2,2,2);
Dloc{24}(2,:,:,:,2)=1;
Dloc{24}(1,:,:,:,1)=1;

%%% field 25 : base on field 19
Dloc{25}=zeros(2,2,2,2,2);
Dloc{25}(2,:,:,:,2)=1;
Dloc{25}(1,:,:,:,1)=1;



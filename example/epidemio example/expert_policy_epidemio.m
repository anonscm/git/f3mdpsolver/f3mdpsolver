function[Ds,Da,Dloc]=expert_policy_epidemio(k,ps,pi,ps_inf,pi_inf,n)

% function that computes an expert policy for the epidemiological problem :
% if healthy field and percentage of infected neighbours >=k : treat with
% probability ps
% if healthy field and percentage of infected neighbours <k : treat with
% probability ps_inf
% if infected field and percentage of infected neighbours >=k : treat with
% probability pi
% if infected field and percentage of infected neighbours <k : treat with
% probability pi_inf
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% k : threshold for the expert policy (percentage of infected neighbours,
% must be berween 0 and 1)
%% ps, pi,ps_inf,pi_inf : parameters for the expert policy
%% n : side of the grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% Ds, Da : structure of the policy
%% Dloc : factored stochastic policy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


if(k<0 || k>100)
    error('expert_policy_epidemio : k must be between 0 and 100');
elseif(ps<0 || ps>1)
    error('expert_policy_epidemio : ps must be between 0 and 1');
elseif(pi<0 || pi>1)
    error('expert_policy_epidemio : pi must be between 0 and 1');
elseif(ps_inf<0 || ps_inf>1)
    error('expert_policy_epidemio : ps_inf must be between 0 and 1');
elseif(pi_inf<0 || pi_inf>1)
    error('expert_policy_epidemio : pi_inf must be between 0 and 1');
elseif(n<=0)
    error('expert_policy_epidemio : n must be a strictly positive integer');
end

% there are n^2 fields
Dloc=cell(1,n^2);
Da=cell(1,n^2);

Ds=grid_neigh(n);

% process all possible states for the neighbours of field i
for i=1:n^2
    nb_vois=length(Ds{i});
    dim=2*ones(1,nb_vois+1); % for a^t_i
    Dloc{i}=zeros(dim);    % order of the dimensions : a^t_i,s^t_neighbours
    
    % index of field i in its neighbourhood
    Ps=grid_neigh(n);
    ind_i=find(Ps{i}==i,1);
    
    for v=1:(2^nb_vois) % global state
        v_vec=n_vec(v,2*ones(1,nb_vois)); % local states
        v_vec_list=num2cell(v_vec);
        ni=sum(v_vec(2:end)==2); % number of infected fields without i itself

        if(v_vec(ind_i)==1) % i is healthy
            if((ni/nb_vois)>=k) % if percentage of infected neighbours greater than k
                Dloc{i}(2,v_vec_list{:})=ps;
                Dloc{i}(1,v_vec_list{:})=1-ps;
            else
                Dloc{i}(2,v_vec_list{:})=ps_inf;
                Dloc{i}(1,v_vec_list{:})=1-ps_inf;
            end

        else % i is infected
            if((ni/nb_vois)>=k) % if percentage of neighbours greater than k
                Dloc{i}(2,v_vec_list{:})=pi;
                Dloc{i}(1,v_vec_list{:})=1-pi;
            else
                Dloc{i}(2,v_vec_list{:})=pi_inf;
                Dloc{i}(1,v_vec_list{:})=1-pi_inf;
            end
        end
    end
end
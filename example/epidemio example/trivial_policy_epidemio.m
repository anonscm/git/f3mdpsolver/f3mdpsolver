function[Ds,Da,Dloc]=trivial_policy_epidemio(n)

% function that gives a trivial policy for the epidemio problem :
% if healthy site : don't treat
% if infected site : treat
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUT
%% n : size of the grid
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUTS
%% Ds, Da : structure of the policy
%% Dloc : table of the policy


if(n<=0)
    error('expert_policy_epidemio : n must be a strictly positive integer.');
end

% there are n^2 sites
Dloc=cell(1,n^2);
Da=cell(1,n^2);
Ds=cell(1,n^2);


% scan all possible states for the neighbours of i
for i=1:n^2
    
    % index of site i in its neighbourhood
    Ps=grid_neigh(n);
    Ds=Ps;
    ind_i=find(Ps{i}==i,1);
    
    nb_vois=length(Ds{i});
    dim=2*ones(1,nb_vois+1); % for a^t_i
    Dloc{i}=zeros(dim);    % order of the dimensions : a^t_i,s^t_voisins    
      
    
    for v=1:(2^nb_vois) % global state
        v_vec=n_vec(v,2*ones(1,nb_vois)); % local states
        v_vec_list=num2cell(v_vec);

        if(v_vec(ind_i)==1) % i is healthy
            Dloc{i}(1,v_vec_list{:})=1;

        else % i is infected
            Dloc{i}(2,v_vec_list{:})=1;
        end
    end
end
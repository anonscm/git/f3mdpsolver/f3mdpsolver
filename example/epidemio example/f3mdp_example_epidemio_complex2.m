function[f3mdp]=f3mdp_example_epidemio_complex2(n,peps,pc,pc2,q,r,s_pol,fact_rec)

% function that creates the F^3MDP corresponding to the F^3MDP problem (transition depends on the action variables on the neighbours and not just on a_i)
% for a grid of size nxn
% version 2 : treatment only reduces the probability of dispersion of the
% disease to a neighbouring field
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% state 1 : healthy, state 2 : infected
% action 1 : normal, action 2 : treat and leave fallow
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% n : side of the grid
%% peps : probability of contamination without neighbours
%% pc : probability of contamination coming from a neighbour
%% pc2 : probability of contamination coming from a neighbour under treatment
%% q : probability to cure
%% r : reward of a healthy site 
%% s_pol : if =1, the policy is based on the state of neighbours
%         if =2, the policy is based on the state of neighbours and their
%         neighbours
%% fact_rec : division factor for the reward when the field is infected (2
% if we want r/2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% f3mdp : the F^3MDP

f3mdp.S=2*ones(1,n^2);
f3mdp.A=2*ones(1,n^2);

f3mdp.n_states=n^2;
f3mdp.n_actions=n^2;
f3mdp.n_rewards=n^2;

% initial probability : factored and uniform
for i=1:f3mdp.n_states
  f3mdp.P_0{i}=[0.5;0.5];
end

% Matrix of the probability of contamination according to the number of infected
% neighbours 
Pnni=zeros(4,4);
for i=0:4 % nb of infected neighbours that are not treated
    for j=0:4 % nb of infected neighbours that are treated
        Pnni(i+1,j+1)=peps+(1-peps)*(1-(1-pc)^i)+(1-peps)*((1-pc)^i)*(1-(1-pc2)^j);
    end
end
% caution, Pnni(i,j) corresponds to (i-1,j-1)

% Local transitions
% we use the function grid-neigh (in utils) that gives the neighbourhood in
% a grid in the following order : oneself, top, left, right, bellow

% Local rewards : r(xi,ai)

f3mdp.Ps=grid_neigh(n);
f3mdp.Pa=f3mdp.Ps;
f3mdp.Ps2=cell(1,f3mdp.n_states);

% structure of the policy
if(s_pol==1)
    f3mdp.Ds=f3mdp.Ps;
else
    NN=cell(1,f3mdp.n_states);
    for i=1:f3mdp.n_states
        NN{i}=[];
        for j=f3mdp.Ps{i}
            NN{i}=[NN{i},f3mdp.Ps{j}];
        end
        NN{i}=unique(NN{i});
    end
    f3mdp.Ds=NN;
end
f3mdp.Da=cell(1,f3mdp.n_actions);

f3mdp.Rs=cell(1,f3mdp.n_rewards);
f3mdp.Ra=cell(1,f3mdp.n_rewards);

f3mdp.Ploc=cell(1,f3mdp.n_states);
f3mdp.Rloc=cell(1,f3mdp.n_rewards);

for i=1:f3mdp.n_states
    nb_vois=length(f3mdp.Ps{i})+length(f3mdp.Pa{i});
    dim=2*ones(1,nb_vois+1); % +1 for s_i^{t+1}
    f3mdp.Ploc{i}=zeros(dim);    % order of the dimensions : s_i^{t+1},s^t_neighbours (with s^t_i),a^t_neighbours (with a^t_i)
    
    f3mdp.Rs{i}=i;
    f3mdp.Ra{i}=i;
    f3mdp.Rloc{i}=[r,0;r/fact_rec,0]; 
    
    % index of field i in its neighbourhood
    ind_i=find(f3mdp.Ps{i}==i,1);
    
    % process all possible states and actions for the neighbours of field i
    for v=1:(2^(nb_vois)) 
        v_vec=n_vec(v,[f3mdp.S(f3mdp.Ps{i}),f3mdp.A(f3mdp.Pa{i})]);
        v_vec_list=num2cell(v_vec);
                
        s_vec=v_vec(1:length(f3mdp.Ps{i})); % states of the neighbours
        a_vec=v_vec((length(f3mdp.Ps{i})+1):end); % actions of the neighbours
        ni=sum(s_vec==2 & a_vec==1); % number of infected fields that are not treated
        nj=sum(s_vec==2 & a_vec==2); % number of infected fields that are treated
        
        if(s_vec(ind_i)==1) % i is healthy
            if(a_vec(ind_i)==1) % normal action on i
                % probability that field i becomes infected if normal
                % action
                f3mdp.Ploc{i}(2,v_vec_list{:})=Pnni(ni+1,nj+1); 
                % probability that field i stays healthy if normal action
                f3mdp.Ploc{i}(1,v_vec_list{:})=1-Pnni(ni+1,nj+1);
                
            else % treatement on i
                % probability that field i stays healthy if treatment
                f3mdp.Ploc{i}(1,v_vec_list{:})=1;
            end
            
        else % i is infected
            if(a_vec(ind_i)==1) % normal action on i
                % probability that the field stays infected without
                % treatment
                f3mdp.Ploc{i}(2,v_vec_list{:})=1;                
                
            else % treatment on i
                % probability that the field stays infected with treatment
                f3mdp.Ploc{i}(2,v_vec_list{:})=1-q;
                % probability that the field becomes healthy with treatment
                f3mdp.Ploc{i}(1,v_vec_list{:})=q;
            end
            
        end
    end
end
f3mdp.Ra2=cell(1,f3mdp.n_rewards);




function[Dloc]=surveillance_policy_2()

% policy for the 5x5 epidemiologic problem with surveillance network
% consists in treating if 3 or 4 of the fields in the surveillance network
% are infected
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% Dloc : stochastic factored policy

Dloc=cell(1,25);

for i=1:25
   Dloc{i}=zeros(2,2,2,2,2);
   nb_etats_vois=2^4;
   for j=1:nb_etats_vois
      etats_vois=n_vec(j,[2,2,2,2]);
      etats_vois_list=num2cell(etats_vois);
      if(sum(etats_vois==2)>=3) % we have to treat
          Dloc{i}(2,etats_vois_list{:})=1;
      else % don't treat
          Dloc{i}(1,etats_vois_list{:})=1;
      end
   end
end
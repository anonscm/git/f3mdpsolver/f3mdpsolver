function[f3mdp]=f3mdp_example_epidemio(n,peps,pc,q,r,fact_rec)

% function that creates the F^3MDP corresponding to the epidemiological
% problem for a grid of size nxn (this F^3MDP is a GMDP)
% see 'A framework and a mean-field algorithm for the local control of
% spatial processes' (Sabbadin et al. 2012)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% state 1 : healthy, state 2 : infected
% action 1 : normal, action 2 : treat and leave fallow
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INPUTS
%% n : side of the grid
%% peps :  probability of contamination without neighbors
%% pc : probability of contamination coming from a neighbor
%% q : probability to cure
%% r : reward of a healthy site
%% fact_rec : division factor for the reward when the field is infected (2
% if we want r/2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% OUTPUT
%% f3mdp : the F^3MDP
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


f3mdp.S=2*ones(1,n^2);
f3mdp.A=2*ones(1,n^2);

f3mdp.n_states=n^2;
f3mdp.n_actions=n^2;
f3mdp.n_rewards=n^2;

% initial probability : factored and uniform
for i=1:f3mdp.n_states
  f3mdp.P_0{i}=[0.5;0.5];
end

% Vector of the probability of contamination according to the number of infected
% neighbors 
Pnni=[];
for i=0:4 
    P=peps+(1-peps)*(1-(1-pc)^(i));
    Pnni=[Pnni P];
end
% caution, Pnni(i) corresponds to i-1 since the indexes begin at 1 and not
% 0

% Local transitions
% we use the function grid-neigh (in utils) that gives the neighbourhood in
% a grid in the following order : oneself, top, left, right, bellow

% Local rewards : r(xi,ai)
% R depends artificially on states of all the neighbourhood and not just
% field i in order to use the generic function F3mdpToGmdp

f3mdp.Ps=grid_neigh(n);
f3mdp.Ps2=cell(1,f3mdp.n_states);
f3mdp.Rs=f3mdp.Ps;
f3mdp.Ds=f3mdp.Ps;
f3mdp.Da=cell(1,f3mdp.n_actions);

f3mdp.Ploc=cell(1,f3mdp.n_rewards);
f3mdp.Rloc=cell(1,f3mdp.n_rewards);

for i=1:f3mdp.n_states
    f3mdp.Pa{i}=i;    
    nb_vois=length(f3mdp.Ps{i});
    dim=2*ones(1,nb_vois+2); % +2 for s_i^{t+1},a^t_i
    f3mdp.Ploc{i}=zeros(dim);    % order of the dimensions : s_i^{t+1},s^t_neighbours (with s^t_i),a^t_i
    
    f3mdp.Rloc{i}=zeros(2*ones(1,nb_vois+1)); % +1 for a^t_i
    
    % index of field i in its neighbourhood
    ind_i=find(f3mdp.Ps{i}==i,1);
    
    % process all possible states for the neighbours of field i
    for v=1:(2^nb_vois) % global state
        v_vec=n_vec(v,f3mdp.S(f3mdp.Ps{i})); % local states
        v_vec_list=num2cell(v_vec);
        ni=sum(v_vec==2); % number of infected neighbours
        
        if(v_vec(ind_i)==1) % i is healthy
            % prbability that field i becomes infected under a normal
            % action
            f3mdp.Ploc{i}(2,v_vec_list{:},1)=Pnni(ni+1); % ni+1 because vector Pnni is shifted
            % prbability that field i stays healthy if normal action
            f3mdp.Ploc{i}(1,v_vec_list{:},1)=1-Pnni(ni+1); % ni+1 because vector Pnni is shifted
            % probability that field i stays healthy if treatment
            f3mdp.Ploc{i}(1,v_vec_list{:},2)=1;
            
            f3mdp.Rloc{i}(v_vec_list{:},1)=r;
        else % i is infected
            % probability that field i stays infected without treatment
            f3mdp.Ploc{i}(2,v_vec_list{:},1)=1;
            % probability that field i stays infected with treatment
            f3mdp.Ploc{i}(2,v_vec_list{:},2)=1-q;
            % probability that field i becomes healthy under treatment
            f3mdp.Ploc{i}(1,v_vec_list{:},2)=q;
            
            f3mdp.Rloc{i}(v_vec_list{:},1)=r/fact_rec;
        end
    end
end

f3mdp.Ra=f3mdp.Pa;
f3mdp.Ra2=cell(1,f3mdp.n_rewards);


